package com.example.demo;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.util.Arrays;

@Entity
@Table(name = "pg_foreign_data_wrapper", schema = "pg_catalog", catalog = "prueba2")
public class PgForeignDataWrapperEntity {
    private Object fdwacl;
    private byte[] fdwhandler;
    private Object fdwname;
    private Object fdwoptions;
    private byte[] fdwowner;
    private byte[] fdwvalidator;
    private byte[] oid;

    @Basic
    @Column(name = "fdwacl")
    public Object getFdwacl() {
        return fdwacl;
    }

    public void setFdwacl(Object fdwacl) {
        this.fdwacl = fdwacl;
    }

    @Basic
    @Column(name = "fdwhandler")
    public byte[] getFdwhandler() {
        return fdwhandler;
    }

    public void setFdwhandler(byte[] fdwhandler) {
        this.fdwhandler = fdwhandler;
    }

    @Basic
    @Column(name = "fdwname")
    public Object getFdwname() {
        return fdwname;
    }

    public void setFdwname(Object fdwname) {
        this.fdwname = fdwname;
    }

    @Basic
    @Column(name = "fdwoptions")
    public Object getFdwoptions() {
        return fdwoptions;
    }

    public void setFdwoptions(Object fdwoptions) {
        this.fdwoptions = fdwoptions;
    }

    @Basic
    @Column(name = "fdwowner")
    public byte[] getFdwowner() {
        return fdwowner;
    }

    public void setFdwowner(byte[] fdwowner) {
        this.fdwowner = fdwowner;
    }

    @Basic
    @Column(name = "fdwvalidator")
    public byte[] getFdwvalidator() {
        return fdwvalidator;
    }

    public void setFdwvalidator(byte[] fdwvalidator) {
        this.fdwvalidator = fdwvalidator;
    }

    @Basic
    @Column(name = "oid")
    public byte[] getOid() {
        return oid;
    }

    public void setOid(byte[] oid) {
        this.oid = oid;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        PgForeignDataWrapperEntity that = (PgForeignDataWrapperEntity) o;

        if (fdwacl != null ? !fdwacl.equals(that.fdwacl) : that.fdwacl != null) return false;
        if (!Arrays.equals(fdwhandler, that.fdwhandler)) return false;
        if (fdwname != null ? !fdwname.equals(that.fdwname) : that.fdwname != null) return false;
        if (fdwoptions != null ? !fdwoptions.equals(that.fdwoptions) : that.fdwoptions != null) return false;
        if (!Arrays.equals(fdwowner, that.fdwowner)) return false;
        if (!Arrays.equals(fdwvalidator, that.fdwvalidator)) return false;
        if (!Arrays.equals(oid, that.oid)) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = fdwacl != null ? fdwacl.hashCode() : 0;
        result = 31 * result + Arrays.hashCode(fdwhandler);
        result = 31 * result + (fdwname != null ? fdwname.hashCode() : 0);
        result = 31 * result + (fdwoptions != null ? fdwoptions.hashCode() : 0);
        result = 31 * result + Arrays.hashCode(fdwowner);
        result = 31 * result + Arrays.hashCode(fdwvalidator);
        result = 31 * result + Arrays.hashCode(oid);
        return result;
    }
}
