package com.example.demo;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.util.Arrays;

@Entity
@Table(name = "pg_inherits", schema = "pg_catalog", catalog = "postgres")
public class PgInheritsEntity {
    private byte[] inhparent;
    private byte[] inhrelid;
    private int inhseqno;

    @Basic
    @Column(name = "inhparent")
    public byte[] getInhparent() {
        return inhparent;
    }

    public void setInhparent(byte[] inhparent) {
        this.inhparent = inhparent;
    }

    @Basic
    @Column(name = "inhrelid")
    public byte[] getInhrelid() {
        return inhrelid;
    }

    public void setInhrelid(byte[] inhrelid) {
        this.inhrelid = inhrelid;
    }

    @Basic
    @Column(name = "inhseqno")
    public int getInhseqno() {
        return inhseqno;
    }

    public void setInhseqno(int inhseqno) {
        this.inhseqno = inhseqno;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        PgInheritsEntity that = (PgInheritsEntity) o;

        if (inhseqno != that.inhseqno) return false;
        if (!Arrays.equals(inhparent, that.inhparent)) return false;
        if (!Arrays.equals(inhrelid, that.inhrelid)) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = Arrays.hashCode(inhparent);
        result = 31 * result + Arrays.hashCode(inhrelid);
        result = 31 * result + inhseqno;
        return result;
    }
}
