package com.example.demo;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.util.Arrays;

@Entity
@Table(name = "pg_language", schema = "pg_catalog", catalog = "kanellfinal2")
public class PgLanguageEntity {
    private Object lanacl;
    private byte[] laninline;
    private boolean lanispl;
    private Object lanname;
    private byte[] lanowner;
    private byte[] lanplcallfoid;
    private boolean lanpltrusted;
    private byte[] lanvalidator;
    private byte[] oid;

    @Basic
    @Column(name = "lanacl")
    public Object getLanacl() {
        return lanacl;
    }

    public void setLanacl(Object lanacl) {
        this.lanacl = lanacl;
    }

    @Basic
    @Column(name = "laninline")
    public byte[] getLaninline() {
        return laninline;
    }

    public void setLaninline(byte[] laninline) {
        this.laninline = laninline;
    }

    @Basic
    @Column(name = "lanispl")
    public boolean isLanispl() {
        return lanispl;
    }

    public void setLanispl(boolean lanispl) {
        this.lanispl = lanispl;
    }

    @Basic
    @Column(name = "lanname")
    public Object getLanname() {
        return lanname;
    }

    public void setLanname(Object lanname) {
        this.lanname = lanname;
    }

    @Basic
    @Column(name = "lanowner")
    public byte[] getLanowner() {
        return lanowner;
    }

    public void setLanowner(byte[] lanowner) {
        this.lanowner = lanowner;
    }

    @Basic
    @Column(name = "lanplcallfoid")
    public byte[] getLanplcallfoid() {
        return lanplcallfoid;
    }

    public void setLanplcallfoid(byte[] lanplcallfoid) {
        this.lanplcallfoid = lanplcallfoid;
    }

    @Basic
    @Column(name = "lanpltrusted")
    public boolean isLanpltrusted() {
        return lanpltrusted;
    }

    public void setLanpltrusted(boolean lanpltrusted) {
        this.lanpltrusted = lanpltrusted;
    }

    @Basic
    @Column(name = "lanvalidator")
    public byte[] getLanvalidator() {
        return lanvalidator;
    }

    public void setLanvalidator(byte[] lanvalidator) {
        this.lanvalidator = lanvalidator;
    }

    @Basic
    @Column(name = "oid")
    public byte[] getOid() {
        return oid;
    }

    public void setOid(byte[] oid) {
        this.oid = oid;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        PgLanguageEntity that = (PgLanguageEntity) o;

        if (lanispl != that.lanispl) return false;
        if (lanpltrusted != that.lanpltrusted) return false;
        if (lanacl != null ? !lanacl.equals(that.lanacl) : that.lanacl != null) return false;
        if (!Arrays.equals(laninline, that.laninline)) return false;
        if (lanname != null ? !lanname.equals(that.lanname) : that.lanname != null) return false;
        if (!Arrays.equals(lanowner, that.lanowner)) return false;
        if (!Arrays.equals(lanplcallfoid, that.lanplcallfoid)) return false;
        if (!Arrays.equals(lanvalidator, that.lanvalidator)) return false;
        if (!Arrays.equals(oid, that.oid)) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = lanacl != null ? lanacl.hashCode() : 0;
        result = 31 * result + Arrays.hashCode(laninline);
        result = 31 * result + (lanispl ? 1 : 0);
        result = 31 * result + (lanname != null ? lanname.hashCode() : 0);
        result = 31 * result + Arrays.hashCode(lanowner);
        result = 31 * result + Arrays.hashCode(lanplcallfoid);
        result = 31 * result + (lanpltrusted ? 1 : 0);
        result = 31 * result + Arrays.hashCode(lanvalidator);
        result = 31 * result + Arrays.hashCode(oid);
        return result;
    }
}
