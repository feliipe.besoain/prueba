package com.example.demo;

import javax.persistence.*;

@Entity
@Table(name = "clientes_credito", schema = "public", catalog = "kanellfinal2")
public class ClientesCreditoEntity {
    private long id;
    private long mensajes;
    private Integer tipo;
    private Object regsitro;

    @Id
    @Column(name = "id")
    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    @Basic
    @Column(name = "mensajes")
    public long getMensajes() {
        return mensajes;
    }

    public void setMensajes(long mensajes) {
        this.mensajes = mensajes;
    }

    @Basic
    @Column(name = "tipo")
    public Integer getTipo() {
        return tipo;
    }

    public void setTipo(Integer tipo) {
        this.tipo = tipo;
    }

    @Basic
    @Column(name = "regsitro")
    public Object getRegsitro() {
        return regsitro;
    }

    public void setRegsitro(Object regsitro) {
        this.regsitro = regsitro;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        ClientesCreditoEntity that = (ClientesCreditoEntity) o;

        if (id != that.id) return false;
        if (mensajes != that.mensajes) return false;
        if (tipo != null ? !tipo.equals(that.tipo) : that.tipo != null) return false;
        if (regsitro != null ? !regsitro.equals(that.regsitro) : that.regsitro != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = (int) (id ^ (id >>> 32));
        result = 31 * result + (int) (mensajes ^ (mensajes >>> 32));
        result = 31 * result + (tipo != null ? tipo.hashCode() : 0);
        result = 31 * result + (regsitro != null ? regsitro.hashCode() : 0);
        return result;
    }
}
