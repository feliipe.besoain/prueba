package com.example.demo;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.util.Arrays;

@Entity
@Table(name = "pg_foreign_table", schema = "pg_catalog", catalog = "prueba1")
public class PgForeignTableEntity {
    private Object ftoptions;
    private byte[] ftrelid;
    private byte[] ftserver;

    @Basic
    @Column(name = "ftoptions")
    public Object getFtoptions() {
        return ftoptions;
    }

    public void setFtoptions(Object ftoptions) {
        this.ftoptions = ftoptions;
    }

    @Basic
    @Column(name = "ftrelid")
    public byte[] getFtrelid() {
        return ftrelid;
    }

    public void setFtrelid(byte[] ftrelid) {
        this.ftrelid = ftrelid;
    }

    @Basic
    @Column(name = "ftserver")
    public byte[] getFtserver() {
        return ftserver;
    }

    public void setFtserver(byte[] ftserver) {
        this.ftserver = ftserver;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        PgForeignTableEntity that = (PgForeignTableEntity) o;

        if (ftoptions != null ? !ftoptions.equals(that.ftoptions) : that.ftoptions != null) return false;
        if (!Arrays.equals(ftrelid, that.ftrelid)) return false;
        if (!Arrays.equals(ftserver, that.ftserver)) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = ftoptions != null ? ftoptions.hashCode() : 0;
        result = 31 * result + Arrays.hashCode(ftrelid);
        result = 31 * result + Arrays.hashCode(ftserver);
        return result;
    }
}
