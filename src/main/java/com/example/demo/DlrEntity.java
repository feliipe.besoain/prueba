package com.example.demo;

import javax.persistence.*;

@Entity
@Table(name = "dlr", schema = "public", catalog = "kanellfinal2")
public class DlrEntity {
    private long oid;
    private String boxc;
    private String destination;
    private Integer mask;
    private String service;
    private String smsc;
    private String source;
    private Integer status;
    private String ts;
    private String url;

    @Id
    @Column(name = "oid")
    public long getOid() {
        return oid;
    }

    public void setOid(long oid) {
        this.oid = oid;
    }

    @Basic
    @Column(name = "boxc")
    public String getBoxc() {
        return boxc;
    }

    public void setBoxc(String boxc) {
        this.boxc = boxc;
    }

    @Basic
    @Column(name = "destination")
    public String getDestination() {
        return destination;
    }

    public void setDestination(String destination) {
        this.destination = destination;
    }

    @Basic
    @Column(name = "mask")
    public Integer getMask() {
        return mask;
    }

    public void setMask(Integer mask) {
        this.mask = mask;
    }

    @Basic
    @Column(name = "service")
    public String getService() {
        return service;
    }

    public void setService(String service) {
        this.service = service;
    }

    @Basic
    @Column(name = "smsc")
    public String getSmsc() {
        return smsc;
    }

    public void setSmsc(String smsc) {
        this.smsc = smsc;
    }

    @Basic
    @Column(name = "source")
    public String getSource() {
        return source;
    }

    public void setSource(String source) {
        this.source = source;
    }

    @Basic
    @Column(name = "status")
    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    @Basic
    @Column(name = "ts")
    public String getTs() {
        return ts;
    }

    public void setTs(String ts) {
        this.ts = ts;
    }

    @Basic
    @Column(name = "url")
    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        DlrEntity dlrEntity = (DlrEntity) o;

        if (oid != dlrEntity.oid) return false;
        if (boxc != null ? !boxc.equals(dlrEntity.boxc) : dlrEntity.boxc != null) return false;
        if (destination != null ? !destination.equals(dlrEntity.destination) : dlrEntity.destination != null)
            return false;
        if (mask != null ? !mask.equals(dlrEntity.mask) : dlrEntity.mask != null) return false;
        if (service != null ? !service.equals(dlrEntity.service) : dlrEntity.service != null) return false;
        if (smsc != null ? !smsc.equals(dlrEntity.smsc) : dlrEntity.smsc != null) return false;
        if (source != null ? !source.equals(dlrEntity.source) : dlrEntity.source != null) return false;
        if (status != null ? !status.equals(dlrEntity.status) : dlrEntity.status != null) return false;
        if (ts != null ? !ts.equals(dlrEntity.ts) : dlrEntity.ts != null) return false;
        if (url != null ? !url.equals(dlrEntity.url) : dlrEntity.url != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = (int) (oid ^ (oid >>> 32));
        result = 31 * result + (boxc != null ? boxc.hashCode() : 0);
        result = 31 * result + (destination != null ? destination.hashCode() : 0);
        result = 31 * result + (mask != null ? mask.hashCode() : 0);
        result = 31 * result + (service != null ? service.hashCode() : 0);
        result = 31 * result + (smsc != null ? smsc.hashCode() : 0);
        result = 31 * result + (source != null ? source.hashCode() : 0);
        result = 31 * result + (status != null ? status.hashCode() : 0);
        result = 31 * result + (ts != null ? ts.hashCode() : 0);
        result = 31 * result + (url != null ? url.hashCode() : 0);
        return result;
    }
}
