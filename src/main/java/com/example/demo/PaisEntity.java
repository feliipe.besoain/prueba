package com.example.demo;

import javax.persistence.*;

@Entity
@Table(name = "pais", schema = "public", catalog = "kanellfinal2")
public class PaisEntity {
    private int id;
    private String pais;
    private String zonaHorario;
    private String moneda;
    private String prefijo;

    @Id
    @Column(name = "id")
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Basic
    @Column(name = "pais")
    public String getPais() {
        return pais;
    }

    public void setPais(String pais) {
        this.pais = pais;
    }

    @Basic
    @Column(name = "zona_horario")
    public String getZonaHorario() {
        return zonaHorario;
    }

    public void setZonaHorario(String zonaHorario) {
        this.zonaHorario = zonaHorario;
    }

    @Basic
    @Column(name = "moneda")
    public String getMoneda() {
        return moneda;
    }

    public void setMoneda(String moneda) {
        this.moneda = moneda;
    }

    @Basic
    @Column(name = "prefijo")
    public String getPrefijo() {
        return prefijo;
    }

    public void setPrefijo(String prefijo) {
        this.prefijo = prefijo;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        PaisEntity that = (PaisEntity) o;

        if (id != that.id) return false;
        if (pais != null ? !pais.equals(that.pais) : that.pais != null) return false;
        if (zonaHorario != null ? !zonaHorario.equals(that.zonaHorario) : that.zonaHorario != null) return false;
        if (moneda != null ? !moneda.equals(that.moneda) : that.moneda != null) return false;
        if (prefijo != null ? !prefijo.equals(that.prefijo) : that.prefijo != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = id;
        result = 31 * result + (pais != null ? pais.hashCode() : 0);
        result = 31 * result + (zonaHorario != null ? zonaHorario.hashCode() : 0);
        result = 31 * result + (moneda != null ? moneda.hashCode() : 0);
        result = 31 * result + (prefijo != null ? prefijo.hashCode() : 0);
        return result;
    }
}
