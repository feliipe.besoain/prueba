package com.example.demo;

import javax.persistence.*;
import java.sql.Timestamp;

@Entity
@Table(name = "log_facturacion_cliente", schema = "public", catalog = "kanellfinal2")
public class LogFacturacionClienteEntity {
    private long id;
    private Long idFacturacionCliente;
    private String rutEmpresa;
    private String razonSocial;
    private String giro;
    private String direccion;
    private String email;
    private String username;
    private Timestamp fechaRegistro;
    private String otro;
    private Timestamp fechaUltimaModificacion;
    private String mensaje;

    @Id
    @Column(name = "id", nullable = false)
    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    @Basic
    @Column(name = "id_facturacion_cliente", nullable = true)
    public Long getIdFacturacionCliente() {
        return idFacturacionCliente;
    }

    public void setIdFacturacionCliente(Long idFacturacionCliente) {
        this.idFacturacionCliente = idFacturacionCliente;
    }

    @Basic
    @Column(name = "rut_empresa", nullable = true, length = 200)
    public String getRutEmpresa() {
        return rutEmpresa;
    }

    public void setRutEmpresa(String rutEmpresa) {
        this.rutEmpresa = rutEmpresa;
    }

    @Basic
    @Column(name = "razon_social", nullable = true, length = 350)
    public String getRazonSocial() {
        return razonSocial;
    }

    public void setRazonSocial(String razonSocial) {
        this.razonSocial = razonSocial;
    }

    @Basic
    @Column(name = "giro", nullable = true, length = 350)
    public String getGiro() {
        return giro;
    }

    public void setGiro(String giro) {
        this.giro = giro;
    }

    @Basic
    @Column(name = "direccion", nullable = true, length = 250)
    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    @Basic
    @Column(name = "email", nullable = true, length = 50)
    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    @Basic
    @Column(name = "username", nullable = true, length = 50)
    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    @Basic
    @Column(name = "fecha_registro", nullable = true)
    public Timestamp getFechaRegistro() {
        return fechaRegistro;
    }

    public void setFechaRegistro(Timestamp fechaRegistro) {
        this.fechaRegistro = fechaRegistro;
    }

    @Basic
    @Column(name = "otro", nullable = true, length = 255)
    public String getOtro() {
        return otro;
    }

    public void setOtro(String otro) {
        this.otro = otro;
    }

    @Basic
    @Column(name = "fecha_ultima_modificacion", nullable = true)
    public Timestamp getFechaUltimaModificacion() {
        return fechaUltimaModificacion;
    }

    public void setFechaUltimaModificacion(Timestamp fechaUltimaModificacion) {
        this.fechaUltimaModificacion = fechaUltimaModificacion;
    }

    @Basic
    @Column(name = "mensaje", nullable = true, length = 255)
    public String getMensaje() {
        return mensaje;
    }

    public void setMensaje(String mensaje) {
        this.mensaje = mensaje;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        LogFacturacionClienteEntity that = (LogFacturacionClienteEntity) o;

        if (id != that.id) return false;
        if (idFacturacionCliente != null ? !idFacturacionCliente.equals(that.idFacturacionCliente) : that.idFacturacionCliente != null)
            return false;
        if (rutEmpresa != null ? !rutEmpresa.equals(that.rutEmpresa) : that.rutEmpresa != null) return false;
        if (razonSocial != null ? !razonSocial.equals(that.razonSocial) : that.razonSocial != null) return false;
        if (giro != null ? !giro.equals(that.giro) : that.giro != null) return false;
        if (direccion != null ? !direccion.equals(that.direccion) : that.direccion != null) return false;
        if (email != null ? !email.equals(that.email) : that.email != null) return false;
        if (username != null ? !username.equals(that.username) : that.username != null) return false;
        if (fechaRegistro != null ? !fechaRegistro.equals(that.fechaRegistro) : that.fechaRegistro != null)
            return false;
        if (otro != null ? !otro.equals(that.otro) : that.otro != null) return false;
        if (fechaUltimaModificacion != null ? !fechaUltimaModificacion.equals(that.fechaUltimaModificacion) : that.fechaUltimaModificacion != null)
            return false;
        if (mensaje != null ? !mensaje.equals(that.mensaje) : that.mensaje != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = (int) (id ^ (id >>> 32));
        result = 31 * result + (idFacturacionCliente != null ? idFacturacionCliente.hashCode() : 0);
        result = 31 * result + (rutEmpresa != null ? rutEmpresa.hashCode() : 0);
        result = 31 * result + (razonSocial != null ? razonSocial.hashCode() : 0);
        result = 31 * result + (giro != null ? giro.hashCode() : 0);
        result = 31 * result + (direccion != null ? direccion.hashCode() : 0);
        result = 31 * result + (email != null ? email.hashCode() : 0);
        result = 31 * result + (username != null ? username.hashCode() : 0);
        result = 31 * result + (fechaRegistro != null ? fechaRegistro.hashCode() : 0);
        result = 31 * result + (otro != null ? otro.hashCode() : 0);
        result = 31 * result + (fechaUltimaModificacion != null ? fechaUltimaModificacion.hashCode() : 0);
        result = 31 * result + (mensaje != null ? mensaje.hashCode() : 0);
        return result;
    }
}
