package com.example.demo;

import javax.persistence.*;
import java.sql.Date;
import java.sql.Timestamp;

@Entity
@Table(name = "usuarios_notify", schema = "public", catalog = "kanellfinal2")
public class UsuariosNotifyEntity {
    private long id;
    private Timestamp fechaRegistro;
    private Integer limiteCreditos;
    private Date fechaNacimiento;
    private String email;
    private String numeroTelefonico;
    private Integer status;
    private String username;
    private String rut;
    private Timestamp fechaUltimaModificacion;

    @Id
    @Column(name = "id")
    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    @Basic
    @Column(name = "fecha_registro")
    public Timestamp getFechaRegistro() {
        return fechaRegistro;
    }

    public void setFechaRegistro(Timestamp fechaRegistro) {
        this.fechaRegistro = fechaRegistro;
    }

    @Basic
    @Column(name = "limite_creditos")
    public Integer getLimiteCreditos() {
        return limiteCreditos;
    }

    public void setLimiteCreditos(Integer limiteCreditos) {
        this.limiteCreditos = limiteCreditos;
    }

    @Basic
    @Column(name = "fecha_nacimiento")
    public Date getFechaNacimiento() {
        return fechaNacimiento;
    }

    public void setFechaNacimiento(Date fechaNacimiento) {
        this.fechaNacimiento = fechaNacimiento;
    }

    @Basic
    @Column(name = "email")
    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    @Basic
    @Column(name = "numero_telefonico")
    public String getNumeroTelefonico() {
        return numeroTelefonico;
    }

    public void setNumeroTelefonico(String numeroTelefonico) {
        this.numeroTelefonico = numeroTelefonico;
    }

    @Basic
    @Column(name = "status")
    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    @Basic
    @Column(name = "username")
    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    @Basic
    @Column(name = "rut")
    public String getRut() {
        return rut;
    }

    public void setRut(String rut) {
        this.rut = rut;
    }

    @Basic
    @Column(name = "fecha_ultima_modificacion")
    public Timestamp getFechaUltimaModificacion() {
        return fechaUltimaModificacion;
    }

    public void setFechaUltimaModificacion(Timestamp fechaUltimaModificacion) {
        this.fechaUltimaModificacion = fechaUltimaModificacion;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        UsuariosNotifyEntity that = (UsuariosNotifyEntity) o;

        if (id != that.id) return false;
        if (fechaRegistro != null ? !fechaRegistro.equals(that.fechaRegistro) : that.fechaRegistro != null)
            return false;
        if (limiteCreditos != null ? !limiteCreditos.equals(that.limiteCreditos) : that.limiteCreditos != null)
            return false;
        if (fechaNacimiento != null ? !fechaNacimiento.equals(that.fechaNacimiento) : that.fechaNacimiento != null)
            return false;
        if (email != null ? !email.equals(that.email) : that.email != null) return false;
        if (numeroTelefonico != null ? !numeroTelefonico.equals(that.numeroTelefonico) : that.numeroTelefonico != null)
            return false;
        if (status != null ? !status.equals(that.status) : that.status != null) return false;
        if (username != null ? !username.equals(that.username) : that.username != null) return false;
        if (rut != null ? !rut.equals(that.rut) : that.rut != null) return false;
        if (fechaUltimaModificacion != null ? !fechaUltimaModificacion.equals(that.fechaUltimaModificacion) : that.fechaUltimaModificacion != null)
            return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = (int) (id ^ (id >>> 32));
        result = 31 * result + (fechaRegistro != null ? fechaRegistro.hashCode() : 0);
        result = 31 * result + (limiteCreditos != null ? limiteCreditos.hashCode() : 0);
        result = 31 * result + (fechaNacimiento != null ? fechaNacimiento.hashCode() : 0);
        result = 31 * result + (email != null ? email.hashCode() : 0);
        result = 31 * result + (numeroTelefonico != null ? numeroTelefonico.hashCode() : 0);
        result = 31 * result + (status != null ? status.hashCode() : 0);
        result = 31 * result + (username != null ? username.hashCode() : 0);
        result = 31 * result + (rut != null ? rut.hashCode() : 0);
        result = 31 * result + (fechaUltimaModificacion != null ? fechaUltimaModificacion.hashCode() : 0);
        return result;
    }
}
