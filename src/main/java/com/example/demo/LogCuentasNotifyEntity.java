package com.example.demo;

import javax.persistence.*;
import java.sql.Time;
import java.sql.Timestamp;

@Entity
@Table(name = "log_cuentas_notify", schema = "public", catalog = "kanellfinal2")
public class LogCuentasNotifyEntity {
    private long id;
    private Long idCuentasNotify;
    private Long idclientenotify;
    private Long idcliente;
    private Integer idtiporeporte;
    private Integer idtipocliente;
    private Time horarioLimite;
    private Integer limiteSmsSemanal;
    private Integer listaNegra;
    private Integer smsConcatenados;
    private Integer status;
    private Integer prioridad;
    private Integer mo;
    private Integer idtipoCuentaSms;
    private Integer datosMo;
    private Integer idpaisSms;
    private Integer datosDlr;
    private String nombreCuenta;
    private Timestamp fechaRegistro;
    private String username;
    private String did;
    private Timestamp fechaUltimaModificacion;
    private String mensaje;

    @Id
    @Column(name = "id", nullable = false)
    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    @Basic
    @Column(name = "id_cuentas_notify", nullable = true)
    public Long getIdCuentasNotify() {
        return idCuentasNotify;
    }

    public void setIdCuentasNotify(Long idCuentasNotify) {
        this.idCuentasNotify = idCuentasNotify;
    }

    @Basic
    @Column(name = "idclientenotify", nullable = true)
    public Long getIdclientenotify() {
        return idclientenotify;
    }

    public void setIdclientenotify(Long idclientenotify) {
        this.idclientenotify = idclientenotify;
    }

    @Basic
    @Column(name = "idcliente", nullable = true)
    public Long getIdcliente() {
        return idcliente;
    }

    public void setIdcliente(Long idcliente) {
        this.idcliente = idcliente;
    }

    @Basic
    @Column(name = "idtiporeporte", nullable = true)
    public Integer getIdtiporeporte() {
        return idtiporeporte;
    }

    public void setIdtiporeporte(Integer idtiporeporte) {
        this.idtiporeporte = idtiporeporte;
    }

    @Basic
    @Column(name = "idtipocliente", nullable = true)
    public Integer getIdtipocliente() {
        return idtipocliente;
    }

    public void setIdtipocliente(Integer idtipocliente) {
        this.idtipocliente = idtipocliente;
    }

    @Basic
    @Column(name = "horario_limite", nullable = true)
    public Time getHorarioLimite() {
        return horarioLimite;
    }

    public void setHorarioLimite(Time horarioLimite) {
        this.horarioLimite = horarioLimite;
    }

    @Basic
    @Column(name = "limite_sms_semanal", nullable = true)
    public Integer getLimiteSmsSemanal() {
        return limiteSmsSemanal;
    }

    public void setLimiteSmsSemanal(Integer limiteSmsSemanal) {
        this.limiteSmsSemanal = limiteSmsSemanal;
    }

    @Basic
    @Column(name = "lista_negra", nullable = true)
    public Integer getListaNegra() {
        return listaNegra;
    }

    public void setListaNegra(Integer listaNegra) {
        this.listaNegra = listaNegra;
    }

    @Basic
    @Column(name = "sms_concatenados", nullable = true)
    public Integer getSmsConcatenados() {
        return smsConcatenados;
    }

    public void setSmsConcatenados(Integer smsConcatenados) {
        this.smsConcatenados = smsConcatenados;
    }

    @Basic
    @Column(name = "status", nullable = true)
    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    @Basic
    @Column(name = "prioridad", nullable = true)
    public Integer getPrioridad() {
        return prioridad;
    }

    public void setPrioridad(Integer prioridad) {
        this.prioridad = prioridad;
    }

    @Basic
    @Column(name = "mo", nullable = true)
    public Integer getMo() {
        return mo;
    }

    public void setMo(Integer mo) {
        this.mo = mo;
    }

    @Basic
    @Column(name = "idtipo_cuenta_sms", nullable = true)
    public Integer getIdtipoCuentaSms() {
        return idtipoCuentaSms;
    }

    public void setIdtipoCuentaSms(Integer idtipoCuentaSms) {
        this.idtipoCuentaSms = idtipoCuentaSms;
    }

    @Basic
    @Column(name = "datos_mo", nullable = true)
    public Integer getDatosMo() {
        return datosMo;
    }

    public void setDatosMo(Integer datosMo) {
        this.datosMo = datosMo;
    }

    @Basic
    @Column(name = "idpais_sms", nullable = true)
    public Integer getIdpaisSms() {
        return idpaisSms;
    }

    public void setIdpaisSms(Integer idpaisSms) {
        this.idpaisSms = idpaisSms;
    }

    @Basic
    @Column(name = "datos_dlr", nullable = true)
    public Integer getDatosDlr() {
        return datosDlr;
    }

    public void setDatosDlr(Integer datosDlr) {
        this.datosDlr = datosDlr;
    }

    @Basic
    @Column(name = "nombre_cuenta", nullable = true, length = 255)
    public String getNombreCuenta() {
        return nombreCuenta;
    }

    public void setNombreCuenta(String nombreCuenta) {
        this.nombreCuenta = nombreCuenta;
    }

    @Basic
    @Column(name = "fecha_registro", nullable = true)
    public Timestamp getFechaRegistro() {
        return fechaRegistro;
    }

    public void setFechaRegistro(Timestamp fechaRegistro) {
        this.fechaRegistro = fechaRegistro;
    }

    @Basic
    @Column(name = "username", nullable = true, length = 255)
    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    @Basic
    @Column(name = "did", nullable = true, length = 255)
    public String getDid() {
        return did;
    }

    public void setDid(String did) {
        this.did = did;
    }

    @Basic
    @Column(name = "fecha_ultima_modificacion", nullable = true)
    public Timestamp getFechaUltimaModificacion() {
        return fechaUltimaModificacion;
    }

    public void setFechaUltimaModificacion(Timestamp fechaUltimaModificacion) {
        this.fechaUltimaModificacion = fechaUltimaModificacion;
    }

    @Basic
    @Column(name = "mensaje", nullable = true, length = 255)
    public String getMensaje() {
        return mensaje;
    }

    public void setMensaje(String mensaje) {
        this.mensaje = mensaje;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        LogCuentasNotifyEntity that = (LogCuentasNotifyEntity) o;

        if (id != that.id) return false;
        if (idCuentasNotify != null ? !idCuentasNotify.equals(that.idCuentasNotify) : that.idCuentasNotify != null)
            return false;
        if (idclientenotify != null ? !idclientenotify.equals(that.idclientenotify) : that.idclientenotify != null)
            return false;
        if (idcliente != null ? !idcliente.equals(that.idcliente) : that.idcliente != null) return false;
        if (idtiporeporte != null ? !idtiporeporte.equals(that.idtiporeporte) : that.idtiporeporte != null)
            return false;
        if (idtipocliente != null ? !idtipocliente.equals(that.idtipocliente) : that.idtipocliente != null)
            return false;
        if (horarioLimite != null ? !horarioLimite.equals(that.horarioLimite) : that.horarioLimite != null)
            return false;
        if (limiteSmsSemanal != null ? !limiteSmsSemanal.equals(that.limiteSmsSemanal) : that.limiteSmsSemanal != null)
            return false;
        if (listaNegra != null ? !listaNegra.equals(that.listaNegra) : that.listaNegra != null) return false;
        if (smsConcatenados != null ? !smsConcatenados.equals(that.smsConcatenados) : that.smsConcatenados != null)
            return false;
        if (status != null ? !status.equals(that.status) : that.status != null) return false;
        if (prioridad != null ? !prioridad.equals(that.prioridad) : that.prioridad != null) return false;
        if (mo != null ? !mo.equals(that.mo) : that.mo != null) return false;
        if (idtipoCuentaSms != null ? !idtipoCuentaSms.equals(that.idtipoCuentaSms) : that.idtipoCuentaSms != null)
            return false;
        if (datosMo != null ? !datosMo.equals(that.datosMo) : that.datosMo != null) return false;
        if (idpaisSms != null ? !idpaisSms.equals(that.idpaisSms) : that.idpaisSms != null) return false;
        if (datosDlr != null ? !datosDlr.equals(that.datosDlr) : that.datosDlr != null) return false;
        if (nombreCuenta != null ? !nombreCuenta.equals(that.nombreCuenta) : that.nombreCuenta != null) return false;
        if (fechaRegistro != null ? !fechaRegistro.equals(that.fechaRegistro) : that.fechaRegistro != null)
            return false;
        if (username != null ? !username.equals(that.username) : that.username != null) return false;
        if (did != null ? !did.equals(that.did) : that.did != null) return false;
        if (fechaUltimaModificacion != null ? !fechaUltimaModificacion.equals(that.fechaUltimaModificacion) : that.fechaUltimaModificacion != null)
            return false;
        if (mensaje != null ? !mensaje.equals(that.mensaje) : that.mensaje != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = (int) (id ^ (id >>> 32));
        result = 31 * result + (idCuentasNotify != null ? idCuentasNotify.hashCode() : 0);
        result = 31 * result + (idclientenotify != null ? idclientenotify.hashCode() : 0);
        result = 31 * result + (idcliente != null ? idcliente.hashCode() : 0);
        result = 31 * result + (idtiporeporte != null ? idtiporeporte.hashCode() : 0);
        result = 31 * result + (idtipocliente != null ? idtipocliente.hashCode() : 0);
        result = 31 * result + (horarioLimite != null ? horarioLimite.hashCode() : 0);
        result = 31 * result + (limiteSmsSemanal != null ? limiteSmsSemanal.hashCode() : 0);
        result = 31 * result + (listaNegra != null ? listaNegra.hashCode() : 0);
        result = 31 * result + (smsConcatenados != null ? smsConcatenados.hashCode() : 0);
        result = 31 * result + (status != null ? status.hashCode() : 0);
        result = 31 * result + (prioridad != null ? prioridad.hashCode() : 0);
        result = 31 * result + (mo != null ? mo.hashCode() : 0);
        result = 31 * result + (idtipoCuentaSms != null ? idtipoCuentaSms.hashCode() : 0);
        result = 31 * result + (datosMo != null ? datosMo.hashCode() : 0);
        result = 31 * result + (idpaisSms != null ? idpaisSms.hashCode() : 0);
        result = 31 * result + (datosDlr != null ? datosDlr.hashCode() : 0);
        result = 31 * result + (nombreCuenta != null ? nombreCuenta.hashCode() : 0);
        result = 31 * result + (fechaRegistro != null ? fechaRegistro.hashCode() : 0);
        result = 31 * result + (username != null ? username.hashCode() : 0);
        result = 31 * result + (did != null ? did.hashCode() : 0);
        result = 31 * result + (fechaUltimaModificacion != null ? fechaUltimaModificacion.hashCode() : 0);
        result = 31 * result + (mensaje != null ? mensaje.hashCode() : 0);
        return result;
    }
}
