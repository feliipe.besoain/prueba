package com.example.demo;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.util.Arrays;

@Entity
@Table(name = "pg_enum", schema = "pg_catalog", catalog = "kanellfinal2")
public class PgEnumEntity {
    private Object enumlabel;
    private float enumsortorder;
    private byte[] enumtypid;
    private byte[] oid;

    @Basic
    @Column(name = "enumlabel")
    public Object getEnumlabel() {
        return enumlabel;
    }

    public void setEnumlabel(Object enumlabel) {
        this.enumlabel = enumlabel;
    }

    @Basic
    @Column(name = "enumsortorder")
    public float getEnumsortorder() {
        return enumsortorder;
    }

    public void setEnumsortorder(float enumsortorder) {
        this.enumsortorder = enumsortorder;
    }

    @Basic
    @Column(name = "enumtypid")
    public byte[] getEnumtypid() {
        return enumtypid;
    }

    public void setEnumtypid(byte[] enumtypid) {
        this.enumtypid = enumtypid;
    }

    @Basic
    @Column(name = "oid")
    public byte[] getOid() {
        return oid;
    }

    public void setOid(byte[] oid) {
        this.oid = oid;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        PgEnumEntity that = (PgEnumEntity) o;

        if (Float.compare(that.enumsortorder, enumsortorder) != 0) return false;
        if (enumlabel != null ? !enumlabel.equals(that.enumlabel) : that.enumlabel != null) return false;
        if (!Arrays.equals(enumtypid, that.enumtypid)) return false;
        if (!Arrays.equals(oid, that.oid)) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = enumlabel != null ? enumlabel.hashCode() : 0;
        result = 31 * result + (enumsortorder != +0.0f ? Float.floatToIntBits(enumsortorder) : 0);
        result = 31 * result + Arrays.hashCode(enumtypid);
        result = 31 * result + Arrays.hashCode(oid);
        return result;
    }
}
