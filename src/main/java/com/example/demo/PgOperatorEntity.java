package com.example.demo;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.util.Arrays;

@Entity
@Table(name = "pg_operator", schema = "pg_catalog", catalog = "postgres")
public class PgOperatorEntity {
    private byte[] oid;
    private boolean oprcanhash;
    private boolean oprcanmerge;
    private Object oprcode;
    private byte[] oprcom;
    private Object oprjoin;
    private Object oprkind;
    private byte[] oprleft;
    private Object oprname;
    private byte[] oprnamespace;
    private byte[] oprnegate;
    private byte[] oprowner;
    private Object oprrest;
    private byte[] oprresult;
    private byte[] oprright;

    @Basic
    @Column(name = "oid")
    public byte[] getOid() {
        return oid;
    }

    public void setOid(byte[] oid) {
        this.oid = oid;
    }

    @Basic
    @Column(name = "oprcanhash")
    public boolean isOprcanhash() {
        return oprcanhash;
    }

    public void setOprcanhash(boolean oprcanhash) {
        this.oprcanhash = oprcanhash;
    }

    @Basic
    @Column(name = "oprcanmerge")
    public boolean isOprcanmerge() {
        return oprcanmerge;
    }

    public void setOprcanmerge(boolean oprcanmerge) {
        this.oprcanmerge = oprcanmerge;
    }

    @Basic
    @Column(name = "oprcode")
    public Object getOprcode() {
        return oprcode;
    }

    public void setOprcode(Object oprcode) {
        this.oprcode = oprcode;
    }

    @Basic
    @Column(name = "oprcom")
    public byte[] getOprcom() {
        return oprcom;
    }

    public void setOprcom(byte[] oprcom) {
        this.oprcom = oprcom;
    }

    @Basic
    @Column(name = "oprjoin")
    public Object getOprjoin() {
        return oprjoin;
    }

    public void setOprjoin(Object oprjoin) {
        this.oprjoin = oprjoin;
    }

    @Basic
    @Column(name = "oprkind")
    public Object getOprkind() {
        return oprkind;
    }

    public void setOprkind(Object oprkind) {
        this.oprkind = oprkind;
    }

    @Basic
    @Column(name = "oprleft")
    public byte[] getOprleft() {
        return oprleft;
    }

    public void setOprleft(byte[] oprleft) {
        this.oprleft = oprleft;
    }

    @Basic
    @Column(name = "oprname")
    public Object getOprname() {
        return oprname;
    }

    public void setOprname(Object oprname) {
        this.oprname = oprname;
    }

    @Basic
    @Column(name = "oprnamespace")
    public byte[] getOprnamespace() {
        return oprnamespace;
    }

    public void setOprnamespace(byte[] oprnamespace) {
        this.oprnamespace = oprnamespace;
    }

    @Basic
    @Column(name = "oprnegate")
    public byte[] getOprnegate() {
        return oprnegate;
    }

    public void setOprnegate(byte[] oprnegate) {
        this.oprnegate = oprnegate;
    }

    @Basic
    @Column(name = "oprowner")
    public byte[] getOprowner() {
        return oprowner;
    }

    public void setOprowner(byte[] oprowner) {
        this.oprowner = oprowner;
    }

    @Basic
    @Column(name = "oprrest")
    public Object getOprrest() {
        return oprrest;
    }

    public void setOprrest(Object oprrest) {
        this.oprrest = oprrest;
    }

    @Basic
    @Column(name = "oprresult")
    public byte[] getOprresult() {
        return oprresult;
    }

    public void setOprresult(byte[] oprresult) {
        this.oprresult = oprresult;
    }

    @Basic
    @Column(name = "oprright")
    public byte[] getOprright() {
        return oprright;
    }

    public void setOprright(byte[] oprright) {
        this.oprright = oprright;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        PgOperatorEntity that = (PgOperatorEntity) o;

        if (oprcanhash != that.oprcanhash) return false;
        if (oprcanmerge != that.oprcanmerge) return false;
        if (!Arrays.equals(oid, that.oid)) return false;
        if (oprcode != null ? !oprcode.equals(that.oprcode) : that.oprcode != null) return false;
        if (!Arrays.equals(oprcom, that.oprcom)) return false;
        if (oprjoin != null ? !oprjoin.equals(that.oprjoin) : that.oprjoin != null) return false;
        if (oprkind != null ? !oprkind.equals(that.oprkind) : that.oprkind != null) return false;
        if (!Arrays.equals(oprleft, that.oprleft)) return false;
        if (oprname != null ? !oprname.equals(that.oprname) : that.oprname != null) return false;
        if (!Arrays.equals(oprnamespace, that.oprnamespace)) return false;
        if (!Arrays.equals(oprnegate, that.oprnegate)) return false;
        if (!Arrays.equals(oprowner, that.oprowner)) return false;
        if (oprrest != null ? !oprrest.equals(that.oprrest) : that.oprrest != null) return false;
        if (!Arrays.equals(oprresult, that.oprresult)) return false;
        if (!Arrays.equals(oprright, that.oprright)) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = Arrays.hashCode(oid);
        result = 31 * result + (oprcanhash ? 1 : 0);
        result = 31 * result + (oprcanmerge ? 1 : 0);
        result = 31 * result + (oprcode != null ? oprcode.hashCode() : 0);
        result = 31 * result + Arrays.hashCode(oprcom);
        result = 31 * result + (oprjoin != null ? oprjoin.hashCode() : 0);
        result = 31 * result + (oprkind != null ? oprkind.hashCode() : 0);
        result = 31 * result + Arrays.hashCode(oprleft);
        result = 31 * result + (oprname != null ? oprname.hashCode() : 0);
        result = 31 * result + Arrays.hashCode(oprnamespace);
        result = 31 * result + Arrays.hashCode(oprnegate);
        result = 31 * result + Arrays.hashCode(oprowner);
        result = 31 * result + (oprrest != null ? oprrest.hashCode() : 0);
        result = 31 * result + Arrays.hashCode(oprresult);
        result = 31 * result + Arrays.hashCode(oprright);
        return result;
    }
}
