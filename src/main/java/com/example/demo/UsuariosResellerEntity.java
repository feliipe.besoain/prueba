package com.example.demo;

import javax.persistence.*;
import java.sql.Timestamp;

@Entity
@Table(name = "usuarios_reseller", schema = "public", catalog = "kanellfinal2")
public class UsuariosResellerEntity {
    private long id;
    private Long idcuentareseller;
    private Long idcliente;
    private String rut;
    private Timestamp fechaRegistro;
    private Timestamp fechaNacimiento;
    private Integer status;
    private String contactoComercial;
    private String numeroTelefonico;
    private Long idUsuarios;
    private String username;
    private Timestamp fechaUltimaModificacion;

    @Id
    @Column(name = "id")
    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    @Basic
    @Column(name = "idcuentareseller")
    public Long getIdcuentareseller() {
        return idcuentareseller;
    }

    public void setIdcuentareseller(Long idcuentareseller) {
        this.idcuentareseller = idcuentareseller;
    }

    @Basic
    @Column(name = "idcliente")
    public Long getIdcliente() {
        return idcliente;
    }

    public void setIdcliente(Long idcliente) {
        this.idcliente = idcliente;
    }

    @Basic
    @Column(name = "rut")
    public String getRut() {
        return rut;
    }

    public void setRut(String rut) {
        this.rut = rut;
    }

    @Basic
    @Column(name = "fecha_registro")
    public Timestamp getFechaRegistro() {
        return fechaRegistro;
    }

    public void setFechaRegistro(Timestamp fechaRegistro) {
        this.fechaRegistro = fechaRegistro;
    }

    @Basic
    @Column(name = "fecha_nacimiento")
    public Timestamp getFechaNacimiento() {
        return fechaNacimiento;
    }

    public void setFechaNacimiento(Timestamp fechaNacimiento) {
        this.fechaNacimiento = fechaNacimiento;
    }

    @Basic
    @Column(name = "status")
    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    @Basic
    @Column(name = "contacto_comercial")
    public String getContactoComercial() {
        return contactoComercial;
    }

    public void setContactoComercial(String contactoComercial) {
        this.contactoComercial = contactoComercial;
    }

    @Basic
    @Column(name = "numero_telefonico")
    public String getNumeroTelefonico() {
        return numeroTelefonico;
    }

    public void setNumeroTelefonico(String numeroTelefonico) {
        this.numeroTelefonico = numeroTelefonico;
    }

    @Basic
    @Column(name = "id_usuarios")
    public Long getIdUsuarios() {
        return idUsuarios;
    }

    public void setIdUsuarios(Long idUsuarios) {
        this.idUsuarios = idUsuarios;
    }

    @Basic
    @Column(name = "username")
    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    @Basic
    @Column(name = "fecha_ultima_modificacion")
    public Timestamp getFechaUltimaModificacion() {
        return fechaUltimaModificacion;
    }

    public void setFechaUltimaModificacion(Timestamp fechaUltimaModificacion) {
        this.fechaUltimaModificacion = fechaUltimaModificacion;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        UsuariosResellerEntity that = (UsuariosResellerEntity) o;

        if (id != that.id) return false;
        if (idcuentareseller != null ? !idcuentareseller.equals(that.idcuentareseller) : that.idcuentareseller != null)
            return false;
        if (idcliente != null ? !idcliente.equals(that.idcliente) : that.idcliente != null) return false;
        if (rut != null ? !rut.equals(that.rut) : that.rut != null) return false;
        if (fechaRegistro != null ? !fechaRegistro.equals(that.fechaRegistro) : that.fechaRegistro != null)
            return false;
        if (fechaNacimiento != null ? !fechaNacimiento.equals(that.fechaNacimiento) : that.fechaNacimiento != null)
            return false;
        if (status != null ? !status.equals(that.status) : that.status != null) return false;
        if (contactoComercial != null ? !contactoComercial.equals(that.contactoComercial) : that.contactoComercial != null)
            return false;
        if (numeroTelefonico != null ? !numeroTelefonico.equals(that.numeroTelefonico) : that.numeroTelefonico != null)
            return false;
        if (idUsuarios != null ? !idUsuarios.equals(that.idUsuarios) : that.idUsuarios != null) return false;
        if (username != null ? !username.equals(that.username) : that.username != null) return false;
        if (fechaUltimaModificacion != null ? !fechaUltimaModificacion.equals(that.fechaUltimaModificacion) : that.fechaUltimaModificacion != null)
            return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = (int) (id ^ (id >>> 32));
        result = 31 * result + (idcuentareseller != null ? idcuentareseller.hashCode() : 0);
        result = 31 * result + (idcliente != null ? idcliente.hashCode() : 0);
        result = 31 * result + (rut != null ? rut.hashCode() : 0);
        result = 31 * result + (fechaRegistro != null ? fechaRegistro.hashCode() : 0);
        result = 31 * result + (fechaNacimiento != null ? fechaNacimiento.hashCode() : 0);
        result = 31 * result + (status != null ? status.hashCode() : 0);
        result = 31 * result + (contactoComercial != null ? contactoComercial.hashCode() : 0);
        result = 31 * result + (numeroTelefonico != null ? numeroTelefonico.hashCode() : 0);
        result = 31 * result + (idUsuarios != null ? idUsuarios.hashCode() : 0);
        result = 31 * result + (username != null ? username.hashCode() : 0);
        result = 31 * result + (fechaUltimaModificacion != null ? fechaUltimaModificacion.hashCode() : 0);
        return result;
    }
}
