package com.example.demo;

import javax.persistence.*;

@Entity
@Table(name = "carga_lista_negra", schema = "public", catalog = "kanellfinal2")
public class CargaListaNegraEntity {
    private long id;
    private Long idProceso;
    private String numero;

    @Id
    @Column(name = "id")
    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    @Basic
    @Column(name = "id_proceso")
    public Long getIdProceso() {
        return idProceso;
    }

    public void setIdProceso(Long idProceso) {
        this.idProceso = idProceso;
    }

    @Basic
    @Column(name = "numero")
    public String getNumero() {
        return numero;
    }

    public void setNumero(String numero) {
        this.numero = numero;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        CargaListaNegraEntity that = (CargaListaNegraEntity) o;

        if (id != that.id) return false;
        if (idProceso != null ? !idProceso.equals(that.idProceso) : that.idProceso != null) return false;
        if (numero != null ? !numero.equals(that.numero) : that.numero != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = (int) (id ^ (id >>> 32));
        result = 31 * result + (idProceso != null ? idProceso.hashCode() : 0);
        result = 31 * result + (numero != null ? numero.hashCode() : 0);
        return result;
    }
}
