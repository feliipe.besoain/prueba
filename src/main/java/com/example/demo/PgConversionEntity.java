package com.example.demo;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.util.Arrays;

@Entity
@Table(name = "pg_conversion", schema = "pg_catalog", catalog = "prueba2")
public class PgConversionEntity {
    private boolean condefault;
    private int conforencoding;
    private Object conname;
    private byte[] connamespace;
    private byte[] conowner;
    private Object conproc;
    private int contoencoding;
    private byte[] oid;

    @Basic
    @Column(name = "condefault")
    public boolean isCondefault() {
        return condefault;
    }

    public void setCondefault(boolean condefault) {
        this.condefault = condefault;
    }

    @Basic
    @Column(name = "conforencoding")
    public int getConforencoding() {
        return conforencoding;
    }

    public void setConforencoding(int conforencoding) {
        this.conforencoding = conforencoding;
    }

    @Basic
    @Column(name = "conname")
    public Object getConname() {
        return conname;
    }

    public void setConname(Object conname) {
        this.conname = conname;
    }

    @Basic
    @Column(name = "connamespace")
    public byte[] getConnamespace() {
        return connamespace;
    }

    public void setConnamespace(byte[] connamespace) {
        this.connamespace = connamespace;
    }

    @Basic
    @Column(name = "conowner")
    public byte[] getConowner() {
        return conowner;
    }

    public void setConowner(byte[] conowner) {
        this.conowner = conowner;
    }

    @Basic
    @Column(name = "conproc")
    public Object getConproc() {
        return conproc;
    }

    public void setConproc(Object conproc) {
        this.conproc = conproc;
    }

    @Basic
    @Column(name = "contoencoding")
    public int getContoencoding() {
        return contoencoding;
    }

    public void setContoencoding(int contoencoding) {
        this.contoencoding = contoencoding;
    }

    @Basic
    @Column(name = "oid")
    public byte[] getOid() {
        return oid;
    }

    public void setOid(byte[] oid) {
        this.oid = oid;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        PgConversionEntity that = (PgConversionEntity) o;

        if (condefault != that.condefault) return false;
        if (conforencoding != that.conforencoding) return false;
        if (contoencoding != that.contoencoding) return false;
        if (conname != null ? !conname.equals(that.conname) : that.conname != null) return false;
        if (!Arrays.equals(connamespace, that.connamespace)) return false;
        if (!Arrays.equals(conowner, that.conowner)) return false;
        if (conproc != null ? !conproc.equals(that.conproc) : that.conproc != null) return false;
        if (!Arrays.equals(oid, that.oid)) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = (condefault ? 1 : 0);
        result = 31 * result + conforencoding;
        result = 31 * result + (conname != null ? conname.hashCode() : 0);
        result = 31 * result + Arrays.hashCode(connamespace);
        result = 31 * result + Arrays.hashCode(conowner);
        result = 31 * result + (conproc != null ? conproc.hashCode() : 0);
        result = 31 * result + contoencoding;
        result = 31 * result + Arrays.hashCode(oid);
        return result;
    }
}
