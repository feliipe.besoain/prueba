package com.example.demo;

import javax.persistence.*;
import java.sql.Date;
import java.sql.Timestamp;

@Entity
@Table(name = "cliente_reseller", schema = "public", catalog = "kanellfinal2")
public class ClienteResellerEntity {
    private int id;
    private String nombreEmpresa;
    private String rutEmpresa;
    private Integer status;
    private String direccionEmpresa;
    private String contactoComercial;
    private String numeroTelefonico;
    private Date fechaNacimiento;
    private String observaciones;
    private Integer feeMensual;
    private Integer precioSms;
    private String limiteHorario;
    private Long limiteMensajes;
    private Timestamp fechaRegistro;
    private Timestamp fechaUltimaModificacion;
    private Long idTipoFacturacion;
    private Long idTipoLimiteMensajes;
    private String username;
    private Long idCliente;

    @Id
    @Column(name = "id")
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Basic
    @Column(name = "nombre_empresa")
    public String getNombreEmpresa() {
        return nombreEmpresa;
    }

    public void setNombreEmpresa(String nombreEmpresa) {
        this.nombreEmpresa = nombreEmpresa;
    }

    @Basic
    @Column(name = "rut_empresa")
    public String getRutEmpresa() {
        return rutEmpresa;
    }

    public void setRutEmpresa(String rutEmpresa) {
        this.rutEmpresa = rutEmpresa;
    }

    @Basic
    @Column(name = "status")
    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    @Basic
    @Column(name = "direccion_empresa")
    public String getDireccionEmpresa() {
        return direccionEmpresa;
    }

    public void setDireccionEmpresa(String direccionEmpresa) {
        this.direccionEmpresa = direccionEmpresa;
    }

    @Basic
    @Column(name = "contacto_comercial")
    public String getContactoComercial() {
        return contactoComercial;
    }

    public void setContactoComercial(String contactoComercial) {
        this.contactoComercial = contactoComercial;
    }

    @Basic
    @Column(name = "numero_telefonico")
    public String getNumeroTelefonico() {
        return numeroTelefonico;
    }

    public void setNumeroTelefonico(String numeroTelefonico) {
        this.numeroTelefonico = numeroTelefonico;
    }

    @Basic
    @Column(name = "fecha_nacimiento")
    public Date getFechaNacimiento() {
        return fechaNacimiento;
    }

    public void setFechaNacimiento(Date fechaNacimiento) {
        this.fechaNacimiento = fechaNacimiento;
    }

    @Basic
    @Column(name = "observaciones")
    public String getObservaciones() {
        return observaciones;
    }

    public void setObservaciones(String observaciones) {
        this.observaciones = observaciones;
    }

    @Basic
    @Column(name = "fee_mensual")
    public Integer getFeeMensual() {
        return feeMensual;
    }

    public void setFeeMensual(Integer feeMensual) {
        this.feeMensual = feeMensual;
    }

    @Basic
    @Column(name = "precio_sms")
    public Integer getPrecioSms() {
        return precioSms;
    }

    public void setPrecioSms(Integer precioSms) {
        this.precioSms = precioSms;
    }

    @Basic
    @Column(name = "limite_horario")
    public String getLimiteHorario() {
        return limiteHorario;
    }

    public void setLimiteHorario(String limiteHorario) {
        this.limiteHorario = limiteHorario;
    }

    @Basic
    @Column(name = "limite_mensajes")
    public Long getLimiteMensajes() {
        return limiteMensajes;
    }

    public void setLimiteMensajes(Long limiteMensajes) {
        this.limiteMensajes = limiteMensajes;
    }

    @Basic
    @Column(name = "fecha_registro")
    public Timestamp getFechaRegistro() {
        return fechaRegistro;
    }

    public void setFechaRegistro(Timestamp fechaRegistro) {
        this.fechaRegistro = fechaRegistro;
    }

    @Basic
    @Column(name = "fecha_ultima_modificacion")
    public Timestamp getFechaUltimaModificacion() {
        return fechaUltimaModificacion;
    }

    public void setFechaUltimaModificacion(Timestamp fechaUltimaModificacion) {
        this.fechaUltimaModificacion = fechaUltimaModificacion;
    }

    @Basic
    @Column(name = "id_tipo_facturacion")
    public Long getIdTipoFacturacion() {
        return idTipoFacturacion;
    }

    public void setIdTipoFacturacion(Long idTipoFacturacion) {
        this.idTipoFacturacion = idTipoFacturacion;
    }

    @Basic
    @Column(name = "id_tipo_limite_mensajes")
    public Long getIdTipoLimiteMensajes() {
        return idTipoLimiteMensajes;
    }

    public void setIdTipoLimiteMensajes(Long idTipoLimiteMensajes) {
        this.idTipoLimiteMensajes = idTipoLimiteMensajes;
    }

    @Basic
    @Column(name = "username")
    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    @Basic
    @Column(name = "id_cliente")
    public Long getIdCliente() {
        return idCliente;
    }

    public void setIdCliente(Long idCliente) {
        this.idCliente = idCliente;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        ClienteResellerEntity that = (ClienteResellerEntity) o;

        if (id != that.id) return false;
        if (nombreEmpresa != null ? !nombreEmpresa.equals(that.nombreEmpresa) : that.nombreEmpresa != null)
            return false;
        if (rutEmpresa != null ? !rutEmpresa.equals(that.rutEmpresa) : that.rutEmpresa != null) return false;
        if (status != null ? !status.equals(that.status) : that.status != null) return false;
        if (direccionEmpresa != null ? !direccionEmpresa.equals(that.direccionEmpresa) : that.direccionEmpresa != null)
            return false;
        if (contactoComercial != null ? !contactoComercial.equals(that.contactoComercial) : that.contactoComercial != null)
            return false;
        if (numeroTelefonico != null ? !numeroTelefonico.equals(that.numeroTelefonico) : that.numeroTelefonico != null)
            return false;
        if (fechaNacimiento != null ? !fechaNacimiento.equals(that.fechaNacimiento) : that.fechaNacimiento != null)
            return false;
        if (observaciones != null ? !observaciones.equals(that.observaciones) : that.observaciones != null)
            return false;
        if (feeMensual != null ? !feeMensual.equals(that.feeMensual) : that.feeMensual != null) return false;
        if (precioSms != null ? !precioSms.equals(that.precioSms) : that.precioSms != null) return false;
        if (limiteHorario != null ? !limiteHorario.equals(that.limiteHorario) : that.limiteHorario != null)
            return false;
        if (limiteMensajes != null ? !limiteMensajes.equals(that.limiteMensajes) : that.limiteMensajes != null)
            return false;
        if (fechaRegistro != null ? !fechaRegistro.equals(that.fechaRegistro) : that.fechaRegistro != null)
            return false;
        if (fechaUltimaModificacion != null ? !fechaUltimaModificacion.equals(that.fechaUltimaModificacion) : that.fechaUltimaModificacion != null)
            return false;
        if (idTipoFacturacion != null ? !idTipoFacturacion.equals(that.idTipoFacturacion) : that.idTipoFacturacion != null)
            return false;
        if (idTipoLimiteMensajes != null ? !idTipoLimiteMensajes.equals(that.idTipoLimiteMensajes) : that.idTipoLimiteMensajes != null)
            return false;
        if (username != null ? !username.equals(that.username) : that.username != null) return false;
        if (idCliente != null ? !idCliente.equals(that.idCliente) : that.idCliente != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = id;
        result = 31 * result + (nombreEmpresa != null ? nombreEmpresa.hashCode() : 0);
        result = 31 * result + (rutEmpresa != null ? rutEmpresa.hashCode() : 0);
        result = 31 * result + (status != null ? status.hashCode() : 0);
        result = 31 * result + (direccionEmpresa != null ? direccionEmpresa.hashCode() : 0);
        result = 31 * result + (contactoComercial != null ? contactoComercial.hashCode() : 0);
        result = 31 * result + (numeroTelefonico != null ? numeroTelefonico.hashCode() : 0);
        result = 31 * result + (fechaNacimiento != null ? fechaNacimiento.hashCode() : 0);
        result = 31 * result + (observaciones != null ? observaciones.hashCode() : 0);
        result = 31 * result + (feeMensual != null ? feeMensual.hashCode() : 0);
        result = 31 * result + (precioSms != null ? precioSms.hashCode() : 0);
        result = 31 * result + (limiteHorario != null ? limiteHorario.hashCode() : 0);
        result = 31 * result + (limiteMensajes != null ? limiteMensajes.hashCode() : 0);
        result = 31 * result + (fechaRegistro != null ? fechaRegistro.hashCode() : 0);
        result = 31 * result + (fechaUltimaModificacion != null ? fechaUltimaModificacion.hashCode() : 0);
        result = 31 * result + (idTipoFacturacion != null ? idTipoFacturacion.hashCode() : 0);
        result = 31 * result + (idTipoLimiteMensajes != null ? idTipoLimiteMensajes.hashCode() : 0);
        result = 31 * result + (username != null ? username.hashCode() : 0);
        result = 31 * result + (idCliente != null ? idCliente.hashCode() : 0);
        return result;
    }
}
