package com.example.demo;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.sql.Timestamp;

@Entity
@Table(name = "dlr_unitario", schema = "public", catalog = "kanellfinal2")
public class DlrUnitarioEntity {
    private long id;
    private String destination;
    private String smsc;
    private String source;
    private Integer dlr;
    private String ts;
    private Long idsms;
    private Timestamp registro;
    private Timestamp update;

    @Basic
    @Column(name = "id")
    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    @Basic
    @Column(name = "destination")
    public String getDestination() {
        return destination;
    }

    public void setDestination(String destination) {
        this.destination = destination;
    }

    @Basic
    @Column(name = "smsc")
    public String getSmsc() {
        return smsc;
    }

    public void setSmsc(String smsc) {
        this.smsc = smsc;
    }

    @Basic
    @Column(name = "source")
    public String getSource() {
        return source;
    }

    public void setSource(String source) {
        this.source = source;
    }

    @Basic
    @Column(name = "dlr")
    public Integer getDlr() {
        return dlr;
    }

    public void setDlr(Integer dlr) {
        this.dlr = dlr;
    }

    @Basic
    @Column(name = "ts")
    public String getTs() {
        return ts;
    }

    public void setTs(String ts) {
        this.ts = ts;
    }

    @Basic
    @Column(name = "idsms")
    public Long getIdsms() {
        return idsms;
    }

    public void setIdsms(Long idsms) {
        this.idsms = idsms;
    }

    @Basic
    @Column(name = "registro")
    public Timestamp getRegistro() {
        return registro;
    }

    public void setRegistro(Timestamp registro) {
        this.registro = registro;
    }

    @Basic
    @Column(name = "update")
    public Timestamp getUpdate() {
        return update;
    }

    public void setUpdate(Timestamp update) {
        this.update = update;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        DlrUnitarioEntity that = (DlrUnitarioEntity) o;

        if (id != that.id) return false;
        if (destination != null ? !destination.equals(that.destination) : that.destination != null) return false;
        if (smsc != null ? !smsc.equals(that.smsc) : that.smsc != null) return false;
        if (source != null ? !source.equals(that.source) : that.source != null) return false;
        if (dlr != null ? !dlr.equals(that.dlr) : that.dlr != null) return false;
        if (ts != null ? !ts.equals(that.ts) : that.ts != null) return false;
        if (idsms != null ? !idsms.equals(that.idsms) : that.idsms != null) return false;
        if (registro != null ? !registro.equals(that.registro) : that.registro != null) return false;
        if (update != null ? !update.equals(that.update) : that.update != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = (int) (id ^ (id >>> 32));
        result = 31 * result + (destination != null ? destination.hashCode() : 0);
        result = 31 * result + (smsc != null ? smsc.hashCode() : 0);
        result = 31 * result + (source != null ? source.hashCode() : 0);
        result = 31 * result + (dlr != null ? dlr.hashCode() : 0);
        result = 31 * result + (ts != null ? ts.hashCode() : 0);
        result = 31 * result + (idsms != null ? idsms.hashCode() : 0);
        result = 31 * result + (registro != null ? registro.hashCode() : 0);
        result = 31 * result + (update != null ? update.hashCode() : 0);
        return result;
    }
}
