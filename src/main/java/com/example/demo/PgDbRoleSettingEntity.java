package com.example.demo;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.util.Arrays;

@Entity
@Table(name = "pg_db_role_setting", schema = "pg_catalog", catalog = "prueba1")
public class PgDbRoleSettingEntity {
    private Object setconfig;
    private byte[] setdatabase;
    private byte[] setrole;

    @Basic
    @Column(name = "setconfig")
    public Object getSetconfig() {
        return setconfig;
    }

    public void setSetconfig(Object setconfig) {
        this.setconfig = setconfig;
    }

    @Basic
    @Column(name = "setdatabase")
    public byte[] getSetdatabase() {
        return setdatabase;
    }

    public void setSetdatabase(byte[] setdatabase) {
        this.setdatabase = setdatabase;
    }

    @Basic
    @Column(name = "setrole")
    public byte[] getSetrole() {
        return setrole;
    }

    public void setSetrole(byte[] setrole) {
        this.setrole = setrole;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        PgDbRoleSettingEntity that = (PgDbRoleSettingEntity) o;

        if (setconfig != null ? !setconfig.equals(that.setconfig) : that.setconfig != null) return false;
        if (!Arrays.equals(setdatabase, that.setdatabase)) return false;
        if (!Arrays.equals(setrole, that.setrole)) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = setconfig != null ? setconfig.hashCode() : 0;
        result = 31 * result + Arrays.hashCode(setdatabase);
        result = 31 * result + Arrays.hashCode(setrole);
        return result;
    }
}
