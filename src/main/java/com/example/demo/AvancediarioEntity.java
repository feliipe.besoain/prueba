package com.example.demo;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.sql.Date;

@Entity
@Table(name = "avancediario", schema = "public", catalog = "kanellfinal2")
public class AvancediarioEntity {
    private int id;
    private Date dia;
    private Integer meta;
    private Float avance;
    private Integer metadia;
    private Integer llevamos;
    private Integer diferencia;
    private Float avancemeta;
    private Float caida;

    @Basic
    @Column(name = "id")
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Basic
    @Column(name = "dia")
    public Date getDia() {
        return dia;
    }

    public void setDia(Date dia) {
        this.dia = dia;
    }

    @Basic
    @Column(name = "meta")
    public Integer getMeta() {
        return meta;
    }

    public void setMeta(Integer meta) {
        this.meta = meta;
    }

    @Basic
    @Column(name = "avance")
    public Float getAvance() {
        return avance;
    }

    public void setAvance(Float avance) {
        this.avance = avance;
    }

    @Basic
    @Column(name = "metadia")
    public Integer getMetadia() {
        return metadia;
    }

    public void setMetadia(Integer metadia) {
        this.metadia = metadia;
    }

    @Basic
    @Column(name = "llevamos")
    public Integer getLlevamos() {
        return llevamos;
    }

    public void setLlevamos(Integer llevamos) {
        this.llevamos = llevamos;
    }

    @Basic
    @Column(name = "diferencia")
    public Integer getDiferencia() {
        return diferencia;
    }

    public void setDiferencia(Integer diferencia) {
        this.diferencia = diferencia;
    }

    @Basic
    @Column(name = "avancemeta")
    public Float getAvancemeta() {
        return avancemeta;
    }

    public void setAvancemeta(Float avancemeta) {
        this.avancemeta = avancemeta;
    }

    @Basic
    @Column(name = "caida")
    public Float getCaida() {
        return caida;
    }

    public void setCaida(Float caida) {
        this.caida = caida;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        AvancediarioEntity that = (AvancediarioEntity) o;

        if (id != that.id) return false;
        if (dia != null ? !dia.equals(that.dia) : that.dia != null) return false;
        if (meta != null ? !meta.equals(that.meta) : that.meta != null) return false;
        if (avance != null ? !avance.equals(that.avance) : that.avance != null) return false;
        if (metadia != null ? !metadia.equals(that.metadia) : that.metadia != null) return false;
        if (llevamos != null ? !llevamos.equals(that.llevamos) : that.llevamos != null) return false;
        if (diferencia != null ? !diferencia.equals(that.diferencia) : that.diferencia != null) return false;
        if (avancemeta != null ? !avancemeta.equals(that.avancemeta) : that.avancemeta != null) return false;
        if (caida != null ? !caida.equals(that.caida) : that.caida != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = id;
        result = 31 * result + (dia != null ? dia.hashCode() : 0);
        result = 31 * result + (meta != null ? meta.hashCode() : 0);
        result = 31 * result + (avance != null ? avance.hashCode() : 0);
        result = 31 * result + (metadia != null ? metadia.hashCode() : 0);
        result = 31 * result + (llevamos != null ? llevamos.hashCode() : 0);
        result = 31 * result + (diferencia != null ? diferencia.hashCode() : 0);
        result = 31 * result + (avancemeta != null ? avancemeta.hashCode() : 0);
        result = 31 * result + (caida != null ? caida.hashCode() : 0);
        return result;
    }
}
