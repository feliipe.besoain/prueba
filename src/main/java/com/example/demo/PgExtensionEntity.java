package com.example.demo;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.util.Arrays;

@Entity
@Table(name = "pg_extension", schema = "pg_catalog", catalog = "dev")
public class PgExtensionEntity {
    private Object extcondition;
    private Object extconfig;
    private Object extname;
    private byte[] extnamespace;
    private byte[] extowner;
    private boolean extrelocatable;
    private String extversion;
    private byte[] oid;

    @Basic
    @Column(name = "extcondition")
    public Object getExtcondition() {
        return extcondition;
    }

    public void setExtcondition(Object extcondition) {
        this.extcondition = extcondition;
    }

    @Basic
    @Column(name = "extconfig")
    public Object getExtconfig() {
        return extconfig;
    }

    public void setExtconfig(Object extconfig) {
        this.extconfig = extconfig;
    }

    @Basic
    @Column(name = "extname")
    public Object getExtname() {
        return extname;
    }

    public void setExtname(Object extname) {
        this.extname = extname;
    }

    @Basic
    @Column(name = "extnamespace")
    public byte[] getExtnamespace() {
        return extnamespace;
    }

    public void setExtnamespace(byte[] extnamespace) {
        this.extnamespace = extnamespace;
    }

    @Basic
    @Column(name = "extowner")
    public byte[] getExtowner() {
        return extowner;
    }

    public void setExtowner(byte[] extowner) {
        this.extowner = extowner;
    }

    @Basic
    @Column(name = "extrelocatable")
    public boolean isExtrelocatable() {
        return extrelocatable;
    }

    public void setExtrelocatable(boolean extrelocatable) {
        this.extrelocatable = extrelocatable;
    }

    @Basic
    @Column(name = "extversion")
    public String getExtversion() {
        return extversion;
    }

    public void setExtversion(String extversion) {
        this.extversion = extversion;
    }

    @Basic
    @Column(name = "oid")
    public byte[] getOid() {
        return oid;
    }

    public void setOid(byte[] oid) {
        this.oid = oid;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        PgExtensionEntity that = (PgExtensionEntity) o;

        if (extrelocatable != that.extrelocatable) return false;
        if (extcondition != null ? !extcondition.equals(that.extcondition) : that.extcondition != null) return false;
        if (extconfig != null ? !extconfig.equals(that.extconfig) : that.extconfig != null) return false;
        if (extname != null ? !extname.equals(that.extname) : that.extname != null) return false;
        if (!Arrays.equals(extnamespace, that.extnamespace)) return false;
        if (!Arrays.equals(extowner, that.extowner)) return false;
        if (extversion != null ? !extversion.equals(that.extversion) : that.extversion != null) return false;
        if (!Arrays.equals(oid, that.oid)) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = extcondition != null ? extcondition.hashCode() : 0;
        result = 31 * result + (extconfig != null ? extconfig.hashCode() : 0);
        result = 31 * result + (extname != null ? extname.hashCode() : 0);
        result = 31 * result + Arrays.hashCode(extnamespace);
        result = 31 * result + Arrays.hashCode(extowner);
        result = 31 * result + (extrelocatable ? 1 : 0);
        result = 31 * result + (extversion != null ? extversion.hashCode() : 0);
        result = 31 * result + Arrays.hashCode(oid);
        return result;
    }
}
