package com.example.demo;

import javax.persistence.*;
import java.sql.Date;
import java.sql.Timestamp;

@Entity
@Table(name = "log_condiciones_comerciales", schema = "public", catalog = "kanellfinal2")
public class LogCondicionesComercialesEntity {
    private int id;
    private Integer idCondicionesComerciales;
    private Integer idtipocredito;
    private Integer limite;
    private Integer creditoAutomaticos;
    private Integer costoPeso;
    private Integer idNuevoTipoValorSms;
    private Date fechaNuevoValorSms;
    private Integer nuevoValorSms;
    private Integer tipoSms;
    private Integer idtipopago;
    private Integer limiteprepago;
    private Integer limitepostpago;
    private Integer feeMensual;
    private Integer idtipocobro;
    private Integer valorSms;
    private String username;
    private Integer idTipoValorSms;
    private Integer idCuentaNotify;
    private Integer idNuevoTipoPago;
    private Integer nuevoCostoPeso;
    private String usernameNuevoValorSms;
    private String fechaRenovacionCreditos;
    private Timestamp fechaUltimaModificacion;
    private String mensaje;

    @Id
    @Column(name = "id", nullable = false)
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Basic
    @Column(name = "id_condiciones_comerciales", nullable = true)
    public Integer getIdCondicionesComerciales() {
        return idCondicionesComerciales;
    }

    public void setIdCondicionesComerciales(Integer idCondicionesComerciales) {
        this.idCondicionesComerciales = idCondicionesComerciales;
    }

    @Basic
    @Column(name = "idtipocredito", nullable = true)
    public Integer getIdtipocredito() {
        return idtipocredito;
    }

    public void setIdtipocredito(Integer idtipocredito) {
        this.idtipocredito = idtipocredito;
    }

    @Basic
    @Column(name = "limite", nullable = true)
    public Integer getLimite() {
        return limite;
    }

    public void setLimite(Integer limite) {
        this.limite = limite;
    }

    @Basic
    @Column(name = "credito_automaticos", nullable = true)
    public Integer getCreditoAutomaticos() {
        return creditoAutomaticos;
    }

    public void setCreditoAutomaticos(Integer creditoAutomaticos) {
        this.creditoAutomaticos = creditoAutomaticos;
    }

    @Basic
    @Column(name = "costo_peso", nullable = true)
    public Integer getCostoPeso() {
        return costoPeso;
    }

    public void setCostoPeso(Integer costoPeso) {
        this.costoPeso = costoPeso;
    }

    @Basic
    @Column(name = "id_nuevo_tipo_valor_sms", nullable = true)
    public Integer getIdNuevoTipoValorSms() {
        return idNuevoTipoValorSms;
    }

    public void setIdNuevoTipoValorSms(Integer idNuevoTipoValorSms) {
        this.idNuevoTipoValorSms = idNuevoTipoValorSms;
    }

    @Basic
    @Column(name = "fecha_nuevo_valor_sms", nullable = true)
    public Date getFechaNuevoValorSms() {
        return fechaNuevoValorSms;
    }

    public void setFechaNuevoValorSms(Date fechaNuevoValorSms) {
        this.fechaNuevoValorSms = fechaNuevoValorSms;
    }

    @Basic
    @Column(name = "nuevo_valor_sms", nullable = true)
    public Integer getNuevoValorSms() {
        return nuevoValorSms;
    }

    public void setNuevoValorSms(Integer nuevoValorSms) {
        this.nuevoValorSms = nuevoValorSms;
    }

    @Basic
    @Column(name = "tipo_sms", nullable = true)
    public Integer getTipoSms() {
        return tipoSms;
    }

    public void setTipoSms(Integer tipoSms) {
        this.tipoSms = tipoSms;
    }

    @Basic
    @Column(name = "idtipopago", nullable = true)
    public Integer getIdtipopago() {
        return idtipopago;
    }

    public void setIdtipopago(Integer idtipopago) {
        this.idtipopago = idtipopago;
    }

    @Basic
    @Column(name = "limiteprepago", nullable = true)
    public Integer getLimiteprepago() {
        return limiteprepago;
    }

    public void setLimiteprepago(Integer limiteprepago) {
        this.limiteprepago = limiteprepago;
    }

    @Basic
    @Column(name = "limitepostpago", nullable = true)
    public Integer getLimitepostpago() {
        return limitepostpago;
    }

    public void setLimitepostpago(Integer limitepostpago) {
        this.limitepostpago = limitepostpago;
    }

    @Basic
    @Column(name = "fee_mensual", nullable = true)
    public Integer getFeeMensual() {
        return feeMensual;
    }

    public void setFeeMensual(Integer feeMensual) {
        this.feeMensual = feeMensual;
    }

    @Basic
    @Column(name = "idtipocobro", nullable = true)
    public Integer getIdtipocobro() {
        return idtipocobro;
    }

    public void setIdtipocobro(Integer idtipocobro) {
        this.idtipocobro = idtipocobro;
    }

    @Basic
    @Column(name = "valor_sms", nullable = true)
    public Integer getValorSms() {
        return valorSms;
    }

    public void setValorSms(Integer valorSms) {
        this.valorSms = valorSms;
    }

    @Basic
    @Column(name = "username", nullable = true, length = 255)
    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    @Basic
    @Column(name = "id_tipo_valor_sms", nullable = true)
    public Integer getIdTipoValorSms() {
        return idTipoValorSms;
    }

    public void setIdTipoValorSms(Integer idTipoValorSms) {
        this.idTipoValorSms = idTipoValorSms;
    }

    @Basic
    @Column(name = "id_cuenta_notify", nullable = true)
    public Integer getIdCuentaNotify() {
        return idCuentaNotify;
    }

    public void setIdCuentaNotify(Integer idCuentaNotify) {
        this.idCuentaNotify = idCuentaNotify;
    }

    @Basic
    @Column(name = "id_nuevo_tipo_pago", nullable = true)
    public Integer getIdNuevoTipoPago() {
        return idNuevoTipoPago;
    }

    public void setIdNuevoTipoPago(Integer idNuevoTipoPago) {
        this.idNuevoTipoPago = idNuevoTipoPago;
    }

    @Basic
    @Column(name = "nuevo_costo_peso", nullable = true)
    public Integer getNuevoCostoPeso() {
        return nuevoCostoPeso;
    }

    public void setNuevoCostoPeso(Integer nuevoCostoPeso) {
        this.nuevoCostoPeso = nuevoCostoPeso;
    }

    @Basic
    @Column(name = "username_nuevo_valor_sms", nullable = true, length = 255)
    public String getUsernameNuevoValorSms() {
        return usernameNuevoValorSms;
    }

    public void setUsernameNuevoValorSms(String usernameNuevoValorSms) {
        this.usernameNuevoValorSms = usernameNuevoValorSms;
    }

    @Basic
    @Column(name = "fecha_renovacion_creditos", nullable = true, length = 255)
    public String getFechaRenovacionCreditos() {
        return fechaRenovacionCreditos;
    }

    public void setFechaRenovacionCreditos(String fechaRenovacionCreditos) {
        this.fechaRenovacionCreditos = fechaRenovacionCreditos;
    }

    @Basic
    @Column(name = "fecha_ultima_modificacion", nullable = true)
    public Timestamp getFechaUltimaModificacion() {
        return fechaUltimaModificacion;
    }

    public void setFechaUltimaModificacion(Timestamp fechaUltimaModificacion) {
        this.fechaUltimaModificacion = fechaUltimaModificacion;
    }

    @Basic
    @Column(name = "mensaje", nullable = true, length = 255)
    public String getMensaje() {
        return mensaje;
    }

    public void setMensaje(String mensaje) {
        this.mensaje = mensaje;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        LogCondicionesComercialesEntity that = (LogCondicionesComercialesEntity) o;

        if (id != that.id) return false;
        if (idCondicionesComerciales != null ? !idCondicionesComerciales.equals(that.idCondicionesComerciales) : that.idCondicionesComerciales != null)
            return false;
        if (idtipocredito != null ? !idtipocredito.equals(that.idtipocredito) : that.idtipocredito != null)
            return false;
        if (limite != null ? !limite.equals(that.limite) : that.limite != null) return false;
        if (creditoAutomaticos != null ? !creditoAutomaticos.equals(that.creditoAutomaticos) : that.creditoAutomaticos != null)
            return false;
        if (costoPeso != null ? !costoPeso.equals(that.costoPeso) : that.costoPeso != null) return false;
        if (idNuevoTipoValorSms != null ? !idNuevoTipoValorSms.equals(that.idNuevoTipoValorSms) : that.idNuevoTipoValorSms != null)
            return false;
        if (fechaNuevoValorSms != null ? !fechaNuevoValorSms.equals(that.fechaNuevoValorSms) : that.fechaNuevoValorSms != null)
            return false;
        if (nuevoValorSms != null ? !nuevoValorSms.equals(that.nuevoValorSms) : that.nuevoValorSms != null)
            return false;
        if (tipoSms != null ? !tipoSms.equals(that.tipoSms) : that.tipoSms != null) return false;
        if (idtipopago != null ? !idtipopago.equals(that.idtipopago) : that.idtipopago != null) return false;
        if (limiteprepago != null ? !limiteprepago.equals(that.limiteprepago) : that.limiteprepago != null)
            return false;
        if (limitepostpago != null ? !limitepostpago.equals(that.limitepostpago) : that.limitepostpago != null)
            return false;
        if (feeMensual != null ? !feeMensual.equals(that.feeMensual) : that.feeMensual != null) return false;
        if (idtipocobro != null ? !idtipocobro.equals(that.idtipocobro) : that.idtipocobro != null) return false;
        if (valorSms != null ? !valorSms.equals(that.valorSms) : that.valorSms != null) return false;
        if (username != null ? !username.equals(that.username) : that.username != null) return false;
        if (idTipoValorSms != null ? !idTipoValorSms.equals(that.idTipoValorSms) : that.idTipoValorSms != null)
            return false;
        if (idCuentaNotify != null ? !idCuentaNotify.equals(that.idCuentaNotify) : that.idCuentaNotify != null)
            return false;
        if (idNuevoTipoPago != null ? !idNuevoTipoPago.equals(that.idNuevoTipoPago) : that.idNuevoTipoPago != null)
            return false;
        if (nuevoCostoPeso != null ? !nuevoCostoPeso.equals(that.nuevoCostoPeso) : that.nuevoCostoPeso != null)
            return false;
        if (usernameNuevoValorSms != null ? !usernameNuevoValorSms.equals(that.usernameNuevoValorSms) : that.usernameNuevoValorSms != null)
            return false;
        if (fechaRenovacionCreditos != null ? !fechaRenovacionCreditos.equals(that.fechaRenovacionCreditos) : that.fechaRenovacionCreditos != null)
            return false;
        if (fechaUltimaModificacion != null ? !fechaUltimaModificacion.equals(that.fechaUltimaModificacion) : that.fechaUltimaModificacion != null)
            return false;
        if (mensaje != null ? !mensaje.equals(that.mensaje) : that.mensaje != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = id;
        result = 31 * result + (idCondicionesComerciales != null ? idCondicionesComerciales.hashCode() : 0);
        result = 31 * result + (idtipocredito != null ? idtipocredito.hashCode() : 0);
        result = 31 * result + (limite != null ? limite.hashCode() : 0);
        result = 31 * result + (creditoAutomaticos != null ? creditoAutomaticos.hashCode() : 0);
        result = 31 * result + (costoPeso != null ? costoPeso.hashCode() : 0);
        result = 31 * result + (idNuevoTipoValorSms != null ? idNuevoTipoValorSms.hashCode() : 0);
        result = 31 * result + (fechaNuevoValorSms != null ? fechaNuevoValorSms.hashCode() : 0);
        result = 31 * result + (nuevoValorSms != null ? nuevoValorSms.hashCode() : 0);
        result = 31 * result + (tipoSms != null ? tipoSms.hashCode() : 0);
        result = 31 * result + (idtipopago != null ? idtipopago.hashCode() : 0);
        result = 31 * result + (limiteprepago != null ? limiteprepago.hashCode() : 0);
        result = 31 * result + (limitepostpago != null ? limitepostpago.hashCode() : 0);
        result = 31 * result + (feeMensual != null ? feeMensual.hashCode() : 0);
        result = 31 * result + (idtipocobro != null ? idtipocobro.hashCode() : 0);
        result = 31 * result + (valorSms != null ? valorSms.hashCode() : 0);
        result = 31 * result + (username != null ? username.hashCode() : 0);
        result = 31 * result + (idTipoValorSms != null ? idTipoValorSms.hashCode() : 0);
        result = 31 * result + (idCuentaNotify != null ? idCuentaNotify.hashCode() : 0);
        result = 31 * result + (idNuevoTipoPago != null ? idNuevoTipoPago.hashCode() : 0);
        result = 31 * result + (nuevoCostoPeso != null ? nuevoCostoPeso.hashCode() : 0);
        result = 31 * result + (usernameNuevoValorSms != null ? usernameNuevoValorSms.hashCode() : 0);
        result = 31 * result + (fechaRenovacionCreditos != null ? fechaRenovacionCreditos.hashCode() : 0);
        result = 31 * result + (fechaUltimaModificacion != null ? fechaUltimaModificacion.hashCode() : 0);
        result = 31 * result + (mensaje != null ? mensaje.hashCode() : 0);
        return result;
    }
}
