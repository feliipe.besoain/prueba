package com.example.demo;

import javax.persistence.*;
import java.sql.Timestamp;

@Entity
@Table(name = "casos_clientes", schema = "public", catalog = "kanellfinal2")
public class CasosClientesEntity {
    private int id;
    private String antecedente;
    private Timestamp fechaRegistro;
    private String username;
    private String pendientes;
    private String notas;

    @Id
    @Column(name = "id")
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Basic
    @Column(name = "antecedente")
    public String getAntecedente() {
        return antecedente;
    }

    public void setAntecedente(String antecedente) {
        this.antecedente = antecedente;
    }

    @Basic
    @Column(name = "fecha_registro")
    public Timestamp getFechaRegistro() {
        return fechaRegistro;
    }

    public void setFechaRegistro(Timestamp fechaRegistro) {
        this.fechaRegistro = fechaRegistro;
    }

    @Basic
    @Column(name = "username")
    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    @Basic
    @Column(name = "pendientes")
    public String getPendientes() {
        return pendientes;
    }

    public void setPendientes(String pendientes) {
        this.pendientes = pendientes;
    }

    @Basic
    @Column(name = "notas")
    public String getNotas() {
        return notas;
    }

    public void setNotas(String notas) {
        this.notas = notas;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        CasosClientesEntity that = (CasosClientesEntity) o;

        if (id != that.id) return false;
        if (antecedente != null ? !antecedente.equals(that.antecedente) : that.antecedente != null) return false;
        if (fechaRegistro != null ? !fechaRegistro.equals(that.fechaRegistro) : that.fechaRegistro != null)
            return false;
        if (username != null ? !username.equals(that.username) : that.username != null) return false;
        if (pendientes != null ? !pendientes.equals(that.pendientes) : that.pendientes != null) return false;
        if (notas != null ? !notas.equals(that.notas) : that.notas != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = id;
        result = 31 * result + (antecedente != null ? antecedente.hashCode() : 0);
        result = 31 * result + (fechaRegistro != null ? fechaRegistro.hashCode() : 0);
        result = 31 * result + (username != null ? username.hashCode() : 0);
        result = 31 * result + (pendientes != null ? pendientes.hashCode() : 0);
        result = 31 * result + (notas != null ? notas.hashCode() : 0);
        return result;
    }
}
