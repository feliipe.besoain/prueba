package com.example.demo;

import javax.persistence.*;
import java.sql.Date;
import java.sql.Timestamp;

@Entity
@Table(name = "log_cuentas_reseller", schema = "public", catalog = "kanellfinal2")
public class LogCuentasResellerEntity {
    private long id;
    private Long idcuentareseller;
    private Long idclientereseller;
    private Long idcliente;
    private String nombreCliente;
    private Integer status;
    private Long idTipoCobro;
    private Integer listaNegra;
    private Long idTipoSms;
    private Timestamp fechaRegistro;
    private Timestamp fechaUltimaModificacion;
    private String username;
    private Integer precioSms;
    private String mensaje;
    private Date fechaAltaNuevoValorSms;
    private Integer nuevoPrecioSms;
    private Integer limiteMensajes;

    @Id
    @Column(name = "id", nullable = false)
    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    @Basic
    @Column(name = "idcuentareseller", nullable = true)
    public Long getIdcuentareseller() {
        return idcuentareseller;
    }

    public void setIdcuentareseller(Long idcuentareseller) {
        this.idcuentareseller = idcuentareseller;
    }

    @Basic
    @Column(name = "idclientereseller", nullable = true)
    public Long getIdclientereseller() {
        return idclientereseller;
    }

    public void setIdclientereseller(Long idclientereseller) {
        this.idclientereseller = idclientereseller;
    }

    @Basic
    @Column(name = "idcliente", nullable = true)
    public Long getIdcliente() {
        return idcliente;
    }

    public void setIdcliente(Long idcliente) {
        this.idcliente = idcliente;
    }

    @Basic
    @Column(name = "nombre_cliente", nullable = true, length = 255)
    public String getNombreCliente() {
        return nombreCliente;
    }

    public void setNombreCliente(String nombreCliente) {
        this.nombreCliente = nombreCliente;
    }

    @Basic
    @Column(name = "status", nullable = true)
    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    @Basic
    @Column(name = "id_tipo_cobro", nullable = true)
    public Long getIdTipoCobro() {
        return idTipoCobro;
    }

    public void setIdTipoCobro(Long idTipoCobro) {
        this.idTipoCobro = idTipoCobro;
    }

    @Basic
    @Column(name = "lista_negra", nullable = true)
    public Integer getListaNegra() {
        return listaNegra;
    }

    public void setListaNegra(Integer listaNegra) {
        this.listaNegra = listaNegra;
    }

    @Basic
    @Column(name = "id_tipo_sms", nullable = true)
    public Long getIdTipoSms() {
        return idTipoSms;
    }

    public void setIdTipoSms(Long idTipoSms) {
        this.idTipoSms = idTipoSms;
    }

    @Basic
    @Column(name = "fecha_registro", nullable = true)
    public Timestamp getFechaRegistro() {
        return fechaRegistro;
    }

    public void setFechaRegistro(Timestamp fechaRegistro) {
        this.fechaRegistro = fechaRegistro;
    }

    @Basic
    @Column(name = "fecha_ultima_modificacion", nullable = true)
    public Timestamp getFechaUltimaModificacion() {
        return fechaUltimaModificacion;
    }

    public void setFechaUltimaModificacion(Timestamp fechaUltimaModificacion) {
        this.fechaUltimaModificacion = fechaUltimaModificacion;
    }

    @Basic
    @Column(name = "username", nullable = true, length = 255)
    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    @Basic
    @Column(name = "precio_sms", nullable = true)
    public Integer getPrecioSms() {
        return precioSms;
    }

    public void setPrecioSms(Integer precioSms) {
        this.precioSms = precioSms;
    }

    @Basic
    @Column(name = "mensaje", nullable = true, length = 255)
    public String getMensaje() {
        return mensaje;
    }

    public void setMensaje(String mensaje) {
        this.mensaje = mensaje;
    }

    @Basic
    @Column(name = "fecha_alta_nuevo_valor_sms", nullable = true)
    public Date getFechaAltaNuevoValorSms() {
        return fechaAltaNuevoValorSms;
    }

    public void setFechaAltaNuevoValorSms(Date fechaAltaNuevoValorSms) {
        this.fechaAltaNuevoValorSms = fechaAltaNuevoValorSms;
    }

    @Basic
    @Column(name = "nuevo_precio_sms", nullable = true)
    public Integer getNuevoPrecioSms() {
        return nuevoPrecioSms;
    }

    public void setNuevoPrecioSms(Integer nuevoPrecioSms) {
        this.nuevoPrecioSms = nuevoPrecioSms;
    }

    @Basic
    @Column(name = "limite_mensajes", nullable = true)
    public Integer getLimiteMensajes() {
        return limiteMensajes;
    }

    public void setLimiteMensajes(Integer limiteMensajes) {
        this.limiteMensajes = limiteMensajes;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        LogCuentasResellerEntity that = (LogCuentasResellerEntity) o;

        if (id != that.id) return false;
        if (idcuentareseller != null ? !idcuentareseller.equals(that.idcuentareseller) : that.idcuentareseller != null)
            return false;
        if (idclientereseller != null ? !idclientereseller.equals(that.idclientereseller) : that.idclientereseller != null)
            return false;
        if (idcliente != null ? !idcliente.equals(that.idcliente) : that.idcliente != null) return false;
        if (nombreCliente != null ? !nombreCliente.equals(that.nombreCliente) : that.nombreCliente != null)
            return false;
        if (status != null ? !status.equals(that.status) : that.status != null) return false;
        if (idTipoCobro != null ? !idTipoCobro.equals(that.idTipoCobro) : that.idTipoCobro != null) return false;
        if (listaNegra != null ? !listaNegra.equals(that.listaNegra) : that.listaNegra != null) return false;
        if (idTipoSms != null ? !idTipoSms.equals(that.idTipoSms) : that.idTipoSms != null) return false;
        if (fechaRegistro != null ? !fechaRegistro.equals(that.fechaRegistro) : that.fechaRegistro != null)
            return false;
        if (fechaUltimaModificacion != null ? !fechaUltimaModificacion.equals(that.fechaUltimaModificacion) : that.fechaUltimaModificacion != null)
            return false;
        if (username != null ? !username.equals(that.username) : that.username != null) return false;
        if (precioSms != null ? !precioSms.equals(that.precioSms) : that.precioSms != null) return false;
        if (mensaje != null ? !mensaje.equals(that.mensaje) : that.mensaje != null) return false;
        if (fechaAltaNuevoValorSms != null ? !fechaAltaNuevoValorSms.equals(that.fechaAltaNuevoValorSms) : that.fechaAltaNuevoValorSms != null)
            return false;
        if (nuevoPrecioSms != null ? !nuevoPrecioSms.equals(that.nuevoPrecioSms) : that.nuevoPrecioSms != null)
            return false;
        if (limiteMensajes != null ? !limiteMensajes.equals(that.limiteMensajes) : that.limiteMensajes != null)
            return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = (int) (id ^ (id >>> 32));
        result = 31 * result + (idcuentareseller != null ? idcuentareseller.hashCode() : 0);
        result = 31 * result + (idclientereseller != null ? idclientereseller.hashCode() : 0);
        result = 31 * result + (idcliente != null ? idcliente.hashCode() : 0);
        result = 31 * result + (nombreCliente != null ? nombreCliente.hashCode() : 0);
        result = 31 * result + (status != null ? status.hashCode() : 0);
        result = 31 * result + (idTipoCobro != null ? idTipoCobro.hashCode() : 0);
        result = 31 * result + (listaNegra != null ? listaNegra.hashCode() : 0);
        result = 31 * result + (idTipoSms != null ? idTipoSms.hashCode() : 0);
        result = 31 * result + (fechaRegistro != null ? fechaRegistro.hashCode() : 0);
        result = 31 * result + (fechaUltimaModificacion != null ? fechaUltimaModificacion.hashCode() : 0);
        result = 31 * result + (username != null ? username.hashCode() : 0);
        result = 31 * result + (precioSms != null ? precioSms.hashCode() : 0);
        result = 31 * result + (mensaje != null ? mensaje.hashCode() : 0);
        result = 31 * result + (fechaAltaNuevoValorSms != null ? fechaAltaNuevoValorSms.hashCode() : 0);
        result = 31 * result + (nuevoPrecioSms != null ? nuevoPrecioSms.hashCode() : 0);
        result = 31 * result + (limiteMensajes != null ? limiteMensajes.hashCode() : 0);
        return result;
    }
}
