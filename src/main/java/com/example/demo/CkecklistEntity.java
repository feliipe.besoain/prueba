package com.example.demo;

import javax.persistence.*;
import java.sql.Timestamp;

@Entity
@Table(name = "ckecklist", schema = "public", catalog = "kanellfinal2")
public class CkecklistEntity {
    private long id;
    private String nombre;
    private String textoSms;
    private Integer conteo;
    private Integer sms;
    private Integer duplicados;
    private Integer envio;
    private Integer malos;
    private Long idcliente;
    private Long idcuenta;
    private Timestamp registro;
    private Timestamp fechaHora;
    private String username;
    private String ruta;

    @Id
    @Column(name = "id")
    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    @Basic
    @Column(name = "nombre")
    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    @Basic
    @Column(name = "texto_sms")
    public String getTextoSms() {
        return textoSms;
    }

    public void setTextoSms(String textoSms) {
        this.textoSms = textoSms;
    }

    @Basic
    @Column(name = "conteo")
    public Integer getConteo() {
        return conteo;
    }

    public void setConteo(Integer conteo) {
        this.conteo = conteo;
    }

    @Basic
    @Column(name = "sms")
    public Integer getSms() {
        return sms;
    }

    public void setSms(Integer sms) {
        this.sms = sms;
    }

    @Basic
    @Column(name = "duplicados")
    public Integer getDuplicados() {
        return duplicados;
    }

    public void setDuplicados(Integer duplicados) {
        this.duplicados = duplicados;
    }

    @Basic
    @Column(name = "envio")
    public Integer getEnvio() {
        return envio;
    }

    public void setEnvio(Integer envio) {
        this.envio = envio;
    }

    @Basic
    @Column(name = "malos")
    public Integer getMalos() {
        return malos;
    }

    public void setMalos(Integer malos) {
        this.malos = malos;
    }

    @Basic
    @Column(name = "idcliente")
    public Long getIdcliente() {
        return idcliente;
    }

    public void setIdcliente(Long idcliente) {
        this.idcliente = idcliente;
    }

    @Basic
    @Column(name = "idcuenta")
    public Long getIdcuenta() {
        return idcuenta;
    }

    public void setIdcuenta(Long idcuenta) {
        this.idcuenta = idcuenta;
    }

    @Basic
    @Column(name = "registro")
    public Timestamp getRegistro() {
        return registro;
    }

    public void setRegistro(Timestamp registro) {
        this.registro = registro;
    }

    @Basic
    @Column(name = "fecha_hora")
    public Timestamp getFechaHora() {
        return fechaHora;
    }

    public void setFechaHora(Timestamp fechaHora) {
        this.fechaHora = fechaHora;
    }

    @Basic
    @Column(name = "username")
    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    @Basic
    @Column(name = "ruta")
    public String getRuta() {
        return ruta;
    }

    public void setRuta(String ruta) {
        this.ruta = ruta;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        CkecklistEntity that = (CkecklistEntity) o;

        if (id != that.id) return false;
        if (nombre != null ? !nombre.equals(that.nombre) : that.nombre != null) return false;
        if (textoSms != null ? !textoSms.equals(that.textoSms) : that.textoSms != null) return false;
        if (conteo != null ? !conteo.equals(that.conteo) : that.conteo != null) return false;
        if (sms != null ? !sms.equals(that.sms) : that.sms != null) return false;
        if (duplicados != null ? !duplicados.equals(that.duplicados) : that.duplicados != null) return false;
        if (envio != null ? !envio.equals(that.envio) : that.envio != null) return false;
        if (malos != null ? !malos.equals(that.malos) : that.malos != null) return false;
        if (idcliente != null ? !idcliente.equals(that.idcliente) : that.idcliente != null) return false;
        if (idcuenta != null ? !idcuenta.equals(that.idcuenta) : that.idcuenta != null) return false;
        if (registro != null ? !registro.equals(that.registro) : that.registro != null) return false;
        if (fechaHora != null ? !fechaHora.equals(that.fechaHora) : that.fechaHora != null) return false;
        if (username != null ? !username.equals(that.username) : that.username != null) return false;
        if (ruta != null ? !ruta.equals(that.ruta) : that.ruta != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = (int) (id ^ (id >>> 32));
        result = 31 * result + (nombre != null ? nombre.hashCode() : 0);
        result = 31 * result + (textoSms != null ? textoSms.hashCode() : 0);
        result = 31 * result + (conteo != null ? conteo.hashCode() : 0);
        result = 31 * result + (sms != null ? sms.hashCode() : 0);
        result = 31 * result + (duplicados != null ? duplicados.hashCode() : 0);
        result = 31 * result + (envio != null ? envio.hashCode() : 0);
        result = 31 * result + (malos != null ? malos.hashCode() : 0);
        result = 31 * result + (idcliente != null ? idcliente.hashCode() : 0);
        result = 31 * result + (idcuenta != null ? idcuenta.hashCode() : 0);
        result = 31 * result + (registro != null ? registro.hashCode() : 0);
        result = 31 * result + (fechaHora != null ? fechaHora.hashCode() : 0);
        result = 31 * result + (username != null ? username.hashCode() : 0);
        result = 31 * result + (ruta != null ? ruta.hashCode() : 0);
        return result;
    }
}
