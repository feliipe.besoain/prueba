package com.example.demo;

import javax.persistence.*;

@Entity
@Table(name = "respuesta_automaticas", schema = "public", catalog = "kanellfinal2")
public class RespuestaAutomaticasEntity {
    private long id;
    private Integer condicionFalsa;
    private Integer condicionVerdadera;
    private String descripcion;
    private String nombre;
    private String regla;
    private String respuestaFalsa;
    private String respuestaVerdadera;
    private Integer status;
    private Integer idRespuestaDestinoFalsa;
    private Integer idRespuestaDestinoVerdadera;
    private String mensajeRespuestaFalsa;
    private String mensajeRespuestaVerdadera;

    @Id
    @Column(name = "id")
    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    @Basic
    @Column(name = "condicion_falsa")
    public Integer getCondicionFalsa() {
        return condicionFalsa;
    }

    public void setCondicionFalsa(Integer condicionFalsa) {
        this.condicionFalsa = condicionFalsa;
    }

    @Basic
    @Column(name = "condicion_verdadera")
    public Integer getCondicionVerdadera() {
        return condicionVerdadera;
    }

    public void setCondicionVerdadera(Integer condicionVerdadera) {
        this.condicionVerdadera = condicionVerdadera;
    }

    @Basic
    @Column(name = "descripcion")
    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    @Basic
    @Column(name = "nombre")
    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    @Basic
    @Column(name = "regla")
    public String getRegla() {
        return regla;
    }

    public void setRegla(String regla) {
        this.regla = regla;
    }

    @Basic
    @Column(name = "respuesta_falsa")
    public String getRespuestaFalsa() {
        return respuestaFalsa;
    }

    public void setRespuestaFalsa(String respuestaFalsa) {
        this.respuestaFalsa = respuestaFalsa;
    }

    @Basic
    @Column(name = "respuesta_verdadera")
    public String getRespuestaVerdadera() {
        return respuestaVerdadera;
    }

    public void setRespuestaVerdadera(String respuestaVerdadera) {
        this.respuestaVerdadera = respuestaVerdadera;
    }

    @Basic
    @Column(name = "status")
    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    @Basic
    @Column(name = "id_respuesta_destino_falsa")
    public Integer getIdRespuestaDestinoFalsa() {
        return idRespuestaDestinoFalsa;
    }

    public void setIdRespuestaDestinoFalsa(Integer idRespuestaDestinoFalsa) {
        this.idRespuestaDestinoFalsa = idRespuestaDestinoFalsa;
    }

    @Basic
    @Column(name = "id_respuesta_destino_verdadera")
    public Integer getIdRespuestaDestinoVerdadera() {
        return idRespuestaDestinoVerdadera;
    }

    public void setIdRespuestaDestinoVerdadera(Integer idRespuestaDestinoVerdadera) {
        this.idRespuestaDestinoVerdadera = idRespuestaDestinoVerdadera;
    }

    @Basic
    @Column(name = "mensaje_respuesta_falsa")
    public String getMensajeRespuestaFalsa() {
        return mensajeRespuestaFalsa;
    }

    public void setMensajeRespuestaFalsa(String mensajeRespuestaFalsa) {
        this.mensajeRespuestaFalsa = mensajeRespuestaFalsa;
    }

    @Basic
    @Column(name = "mensaje_respuesta_verdadera")
    public String getMensajeRespuestaVerdadera() {
        return mensajeRespuestaVerdadera;
    }

    public void setMensajeRespuestaVerdadera(String mensajeRespuestaVerdadera) {
        this.mensajeRespuestaVerdadera = mensajeRespuestaVerdadera;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        RespuestaAutomaticasEntity that = (RespuestaAutomaticasEntity) o;

        if (id != that.id) return false;
        if (condicionFalsa != null ? !condicionFalsa.equals(that.condicionFalsa) : that.condicionFalsa != null)
            return false;
        if (condicionVerdadera != null ? !condicionVerdadera.equals(that.condicionVerdadera) : that.condicionVerdadera != null)
            return false;
        if (descripcion != null ? !descripcion.equals(that.descripcion) : that.descripcion != null) return false;
        if (nombre != null ? !nombre.equals(that.nombre) : that.nombre != null) return false;
        if (regla != null ? !regla.equals(that.regla) : that.regla != null) return false;
        if (respuestaFalsa != null ? !respuestaFalsa.equals(that.respuestaFalsa) : that.respuestaFalsa != null)
            return false;
        if (respuestaVerdadera != null ? !respuestaVerdadera.equals(that.respuestaVerdadera) : that.respuestaVerdadera != null)
            return false;
        if (status != null ? !status.equals(that.status) : that.status != null) return false;
        if (idRespuestaDestinoFalsa != null ? !idRespuestaDestinoFalsa.equals(that.idRespuestaDestinoFalsa) : that.idRespuestaDestinoFalsa != null)
            return false;
        if (idRespuestaDestinoVerdadera != null ? !idRespuestaDestinoVerdadera.equals(that.idRespuestaDestinoVerdadera) : that.idRespuestaDestinoVerdadera != null)
            return false;
        if (mensajeRespuestaFalsa != null ? !mensajeRespuestaFalsa.equals(that.mensajeRespuestaFalsa) : that.mensajeRespuestaFalsa != null)
            return false;
        if (mensajeRespuestaVerdadera != null ? !mensajeRespuestaVerdadera.equals(that.mensajeRespuestaVerdadera) : that.mensajeRespuestaVerdadera != null)
            return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = (int) (id ^ (id >>> 32));
        result = 31 * result + (condicionFalsa != null ? condicionFalsa.hashCode() : 0);
        result = 31 * result + (condicionVerdadera != null ? condicionVerdadera.hashCode() : 0);
        result = 31 * result + (descripcion != null ? descripcion.hashCode() : 0);
        result = 31 * result + (nombre != null ? nombre.hashCode() : 0);
        result = 31 * result + (regla != null ? regla.hashCode() : 0);
        result = 31 * result + (respuestaFalsa != null ? respuestaFalsa.hashCode() : 0);
        result = 31 * result + (respuestaVerdadera != null ? respuestaVerdadera.hashCode() : 0);
        result = 31 * result + (status != null ? status.hashCode() : 0);
        result = 31 * result + (idRespuestaDestinoFalsa != null ? idRespuestaDestinoFalsa.hashCode() : 0);
        result = 31 * result + (idRespuestaDestinoVerdadera != null ? idRespuestaDestinoVerdadera.hashCode() : 0);
        result = 31 * result + (mensajeRespuestaFalsa != null ? mensajeRespuestaFalsa.hashCode() : 0);
        result = 31 * result + (mensajeRespuestaVerdadera != null ? mensajeRespuestaVerdadera.hashCode() : 0);
        return result;
    }
}
