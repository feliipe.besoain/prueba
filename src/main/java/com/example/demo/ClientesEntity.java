package com.example.demo;

import javax.persistence.*;
import java.sql.Timestamp;

@Entity
@Table(name = "clientes", schema = "public", catalog = "kanellfinal2")
public class ClientesEntity {
    private long id;
    private String apellido;
    private String email;
    private String nombre;
    private int prioridad;
    private int status;
    private Integer creditos;
    private Integer nsmsenviados;
    private String urlmt;
    private String urlmo;
    private String usersmpp;
    private String username;
    private String usernamecta;
    private String zonaHoraria;
    private Timestamp fechaUltimaModificacion;
    private UsuariosEntity cliente;

    @Id
    @Column(name = "id")
    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    @Basic
    @Column(name = "apellido")
    public String getApellido() {
        return apellido;
    }

    public void setApellido(String apellido) {
        this.apellido = apellido;
    }

    @Basic
    @Column(name = "email")
    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    @Basic
    @Column(name = "nombre")
    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    @Basic
    @Column(name = "prioridad")
    public int getPrioridad() {
        return prioridad;
    }

    public void setPrioridad(int prioridad) {
        this.prioridad = prioridad;
    }

    @Basic
    @Column(name = "status")
    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    @Basic
    @Column(name = "creditos")
    public Integer getCreditos() {
        return creditos;
    }

    public void setCreditos(Integer creditos) {
        this.creditos = creditos;
    }

    @Basic
    @Column(name = "nsmsenviados")
    public Integer getNsmsenviados() {
        return nsmsenviados;
    }

    public void setNsmsenviados(Integer nsmsenviados) {
        this.nsmsenviados = nsmsenviados;
    }

    @Basic
    @Column(name = "urlmt")
    public String getUrlmt() {
        return urlmt;
    }

    public void setUrlmt(String urlmt) {
        this.urlmt = urlmt;
    }

    @Basic
    @Column(name = "urlmo")
    public String getUrlmo() {
        return urlmo;
    }

    public void setUrlmo(String urlmo) {
        this.urlmo = urlmo;
    }

    @Basic
    @Column(name = "usersmpp")
    public String getUsersmpp() {
        return usersmpp;
    }

    public void setUsersmpp(String usersmpp) {
        this.usersmpp = usersmpp;
    }

    @Basic
    @Column(name = "username")
    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    @Basic
    @Column(name = "usernamecta")
    public String getUsernamecta() {
        return usernamecta;
    }

    public void setUsernamecta(String usernamecta) {
        this.usernamecta = usernamecta;
    }

    @Basic
    @Column(name = "zona_horaria")
    public String getZonaHoraria() {
        return zonaHoraria;
    }

    public void setZonaHoraria(String zonaHoraria) {
        this.zonaHoraria = zonaHoraria;
    }

    @Basic
    @Column(name = "fecha_ultima_modificacion")
    public Timestamp getFechaUltimaModificacion() {
        return fechaUltimaModificacion;
    }

    public void setFechaUltimaModificacion(Timestamp fechaUltimaModificacion) {
        this.fechaUltimaModificacion = fechaUltimaModificacion;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        ClientesEntity that = (ClientesEntity) o;

        if (id != that.id) return false;
        if (prioridad != that.prioridad) return false;
        if (status != that.status) return false;
        if (apellido != null ? !apellido.equals(that.apellido) : that.apellido != null) return false;
        if (email != null ? !email.equals(that.email) : that.email != null) return false;
        if (nombre != null ? !nombre.equals(that.nombre) : that.nombre != null) return false;
        if (creditos != null ? !creditos.equals(that.creditos) : that.creditos != null) return false;
        if (nsmsenviados != null ? !nsmsenviados.equals(that.nsmsenviados) : that.nsmsenviados != null) return false;
        if (urlmt != null ? !urlmt.equals(that.urlmt) : that.urlmt != null) return false;
        if (urlmo != null ? !urlmo.equals(that.urlmo) : that.urlmo != null) return false;
        if (usersmpp != null ? !usersmpp.equals(that.usersmpp) : that.usersmpp != null) return false;
        if (username != null ? !username.equals(that.username) : that.username != null) return false;
        if (usernamecta != null ? !usernamecta.equals(that.usernamecta) : that.usernamecta != null) return false;
        if (zonaHoraria != null ? !zonaHoraria.equals(that.zonaHoraria) : that.zonaHoraria != null) return false;
        if (fechaUltimaModificacion != null ? !fechaUltimaModificacion.equals(that.fechaUltimaModificacion) : that.fechaUltimaModificacion != null)
            return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = (int) (id ^ (id >>> 32));
        result = 31 * result + (apellido != null ? apellido.hashCode() : 0);
        result = 31 * result + (email != null ? email.hashCode() : 0);
        result = 31 * result + (nombre != null ? nombre.hashCode() : 0);
        result = 31 * result + prioridad;
        result = 31 * result + status;
        result = 31 * result + (creditos != null ? creditos.hashCode() : 0);
        result = 31 * result + (nsmsenviados != null ? nsmsenviados.hashCode() : 0);
        result = 31 * result + (urlmt != null ? urlmt.hashCode() : 0);
        result = 31 * result + (urlmo != null ? urlmo.hashCode() : 0);
        result = 31 * result + (usersmpp != null ? usersmpp.hashCode() : 0);
        result = 31 * result + (username != null ? username.hashCode() : 0);
        result = 31 * result + (usernamecta != null ? usernamecta.hashCode() : 0);
        result = 31 * result + (zonaHoraria != null ? zonaHoraria.hashCode() : 0);
        result = 31 * result + (fechaUltimaModificacion != null ? fechaUltimaModificacion.hashCode() : 0);
        return result;
    }

    @OneToOne(mappedBy = "cliente")
    public UsuariosEntity getCliente() {
        return cliente;
    }

    public void setCliente(UsuariosEntity cliente) {
        this.cliente = cliente;
    }
}
