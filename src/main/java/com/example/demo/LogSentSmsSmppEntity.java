package com.example.demo;

import javax.persistence.*;
import java.sql.Timestamp;

@Entity
@Table(name = "log_sent_sms_smpp", schema = "public", catalog = "kanellfinal2")
public class LogSentSmsSmppEntity {
    private long sqlId;
    private String account;
    private Long altDcs;
    private String binfo;
    private String boxcId;
    private String charset;
    private Long coding;
    private Long compress;
    private Long deferred;
    private Long dlrMask;
    private String dlrUrl;
    private String foreignId;
    private Long id;
    private Long mclass;
    private String metaData;
    private String momt;
    private String msgdata;
    private Long mwi;
    private Long pid;
    private String receiver;
    private String sender;
    private String service;
    private Long smsType;
    private String smscId;
    private Long time;
    private String udhdata;
    private Long validity;
    private Long rpi;
    private Timestamp fecharegistro;

    @Id
    @Column(name = "sql_id", nullable = false)
    public long getSqlId() {
        return sqlId;
    }

    public void setSqlId(long sqlId) {
        this.sqlId = sqlId;
    }

    @Basic
    @Column(name = "account", nullable = true, length = 255)
    public String getAccount() {
        return account;
    }

    public void setAccount(String account) {
        this.account = account;
    }

    @Basic
    @Column(name = "alt_dcs", nullable = true)
    public Long getAltDcs() {
        return altDcs;
    }

    public void setAltDcs(Long altDcs) {
        this.altDcs = altDcs;
    }

    @Basic
    @Column(name = "binfo", nullable = true, length = 255)
    public String getBinfo() {
        return binfo;
    }

    public void setBinfo(String binfo) {
        this.binfo = binfo;
    }

    @Basic
    @Column(name = "boxc_id", nullable = true, length = 255)
    public String getBoxcId() {
        return boxcId;
    }

    public void setBoxcId(String boxcId) {
        this.boxcId = boxcId;
    }

    @Basic
    @Column(name = "charset", nullable = true, length = 255)
    public String getCharset() {
        return charset;
    }

    public void setCharset(String charset) {
        this.charset = charset;
    }

    @Basic
    @Column(name = "coding", nullable = true)
    public Long getCoding() {
        return coding;
    }

    public void setCoding(Long coding) {
        this.coding = coding;
    }

    @Basic
    @Column(name = "compress", nullable = true)
    public Long getCompress() {
        return compress;
    }

    public void setCompress(Long compress) {
        this.compress = compress;
    }

    @Basic
    @Column(name = "deferred", nullable = true)
    public Long getDeferred() {
        return deferred;
    }

    public void setDeferred(Long deferred) {
        this.deferred = deferred;
    }

    @Basic
    @Column(name = "dlr_mask", nullable = true)
    public Long getDlrMask() {
        return dlrMask;
    }

    public void setDlrMask(Long dlrMask) {
        this.dlrMask = dlrMask;
    }

    @Basic
    @Column(name = "dlr_url", nullable = true, length = 255)
    public String getDlrUrl() {
        return dlrUrl;
    }

    public void setDlrUrl(String dlrUrl) {
        this.dlrUrl = dlrUrl;
    }

    @Basic
    @Column(name = "foreign_id", nullable = true, length = 255)
    public String getForeignId() {
        return foreignId;
    }

    public void setForeignId(String foreignId) {
        this.foreignId = foreignId;
    }

    @Basic
    @Column(name = "id", nullable = true)
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Basic
    @Column(name = "mclass", nullable = true)
    public Long getMclass() {
        return mclass;
    }

    public void setMclass(Long mclass) {
        this.mclass = mclass;
    }

    @Basic
    @Column(name = "meta_data", nullable = true, length = 1000)
    public String getMetaData() {
        return metaData;
    }

    public void setMetaData(String metaData) {
        this.metaData = metaData;
    }

    @Basic
    @Column(name = "momt", nullable = true, length = 255)
    public String getMomt() {
        return momt;
    }

    public void setMomt(String momt) {
        this.momt = momt;
    }

    @Basic
    @Column(name = "msgdata", nullable = true, length = 1000)
    public String getMsgdata() {
        return msgdata;
    }

    public void setMsgdata(String msgdata) {
        this.msgdata = msgdata;
    }

    @Basic
    @Column(name = "mwi", nullable = true)
    public Long getMwi() {
        return mwi;
    }

    public void setMwi(Long mwi) {
        this.mwi = mwi;
    }

    @Basic
    @Column(name = "pid", nullable = true)
    public Long getPid() {
        return pid;
    }

    public void setPid(Long pid) {
        this.pid = pid;
    }

    @Basic
    @Column(name = "receiver", nullable = true, length = 255)
    public String getReceiver() {
        return receiver;
    }

    public void setReceiver(String receiver) {
        this.receiver = receiver;
    }

    @Basic
    @Column(name = "sender", nullable = true, length = 255)
    public String getSender() {
        return sender;
    }

    public void setSender(String sender) {
        this.sender = sender;
    }

    @Basic
    @Column(name = "service", nullable = true, length = 255)
    public String getService() {
        return service;
    }

    public void setService(String service) {
        this.service = service;
    }

    @Basic
    @Column(name = "sms_type", nullable = true)
    public Long getSmsType() {
        return smsType;
    }

    public void setSmsType(Long smsType) {
        this.smsType = smsType;
    }

    @Basic
    @Column(name = "smsc_id", nullable = true, length = 255)
    public String getSmscId() {
        return smscId;
    }

    public void setSmscId(String smscId) {
        this.smscId = smscId;
    }

    @Basic
    @Column(name = "time", nullable = true)
    public Long getTime() {
        return time;
    }

    public void setTime(Long time) {
        this.time = time;
    }

    @Basic
    @Column(name = "udhdata", nullable = true, length = 255)
    public String getUdhdata() {
        return udhdata;
    }

    public void setUdhdata(String udhdata) {
        this.udhdata = udhdata;
    }

    @Basic
    @Column(name = "validity", nullable = true)
    public Long getValidity() {
        return validity;
    }

    public void setValidity(Long validity) {
        this.validity = validity;
    }

    @Basic
    @Column(name = "rpi", nullable = true)
    public Long getRpi() {
        return rpi;
    }

    public void setRpi(Long rpi) {
        this.rpi = rpi;
    }

    @Basic
    @Column(name = "fecharegistro", nullable = true)
    public Timestamp getFecharegistro() {
        return fecharegistro;
    }

    public void setFecharegistro(Timestamp fecharegistro) {
        this.fecharegistro = fecharegistro;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        LogSentSmsSmppEntity that = (LogSentSmsSmppEntity) o;

        if (sqlId != that.sqlId) return false;
        if (account != null ? !account.equals(that.account) : that.account != null) return false;
        if (altDcs != null ? !altDcs.equals(that.altDcs) : that.altDcs != null) return false;
        if (binfo != null ? !binfo.equals(that.binfo) : that.binfo != null) return false;
        if (boxcId != null ? !boxcId.equals(that.boxcId) : that.boxcId != null) return false;
        if (charset != null ? !charset.equals(that.charset) : that.charset != null) return false;
        if (coding != null ? !coding.equals(that.coding) : that.coding != null) return false;
        if (compress != null ? !compress.equals(that.compress) : that.compress != null) return false;
        if (deferred != null ? !deferred.equals(that.deferred) : that.deferred != null) return false;
        if (dlrMask != null ? !dlrMask.equals(that.dlrMask) : that.dlrMask != null) return false;
        if (dlrUrl != null ? !dlrUrl.equals(that.dlrUrl) : that.dlrUrl != null) return false;
        if (foreignId != null ? !foreignId.equals(that.foreignId) : that.foreignId != null) return false;
        if (id != null ? !id.equals(that.id) : that.id != null) return false;
        if (mclass != null ? !mclass.equals(that.mclass) : that.mclass != null) return false;
        if (metaData != null ? !metaData.equals(that.metaData) : that.metaData != null) return false;
        if (momt != null ? !momt.equals(that.momt) : that.momt != null) return false;
        if (msgdata != null ? !msgdata.equals(that.msgdata) : that.msgdata != null) return false;
        if (mwi != null ? !mwi.equals(that.mwi) : that.mwi != null) return false;
        if (pid != null ? !pid.equals(that.pid) : that.pid != null) return false;
        if (receiver != null ? !receiver.equals(that.receiver) : that.receiver != null) return false;
        if (sender != null ? !sender.equals(that.sender) : that.sender != null) return false;
        if (service != null ? !service.equals(that.service) : that.service != null) return false;
        if (smsType != null ? !smsType.equals(that.smsType) : that.smsType != null) return false;
        if (smscId != null ? !smscId.equals(that.smscId) : that.smscId != null) return false;
        if (time != null ? !time.equals(that.time) : that.time != null) return false;
        if (udhdata != null ? !udhdata.equals(that.udhdata) : that.udhdata != null) return false;
        if (validity != null ? !validity.equals(that.validity) : that.validity != null) return false;
        if (rpi != null ? !rpi.equals(that.rpi) : that.rpi != null) return false;
        if (fecharegistro != null ? !fecharegistro.equals(that.fecharegistro) : that.fecharegistro != null)
            return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = (int) (sqlId ^ (sqlId >>> 32));
        result = 31 * result + (account != null ? account.hashCode() : 0);
        result = 31 * result + (altDcs != null ? altDcs.hashCode() : 0);
        result = 31 * result + (binfo != null ? binfo.hashCode() : 0);
        result = 31 * result + (boxcId != null ? boxcId.hashCode() : 0);
        result = 31 * result + (charset != null ? charset.hashCode() : 0);
        result = 31 * result + (coding != null ? coding.hashCode() : 0);
        result = 31 * result + (compress != null ? compress.hashCode() : 0);
        result = 31 * result + (deferred != null ? deferred.hashCode() : 0);
        result = 31 * result + (dlrMask != null ? dlrMask.hashCode() : 0);
        result = 31 * result + (dlrUrl != null ? dlrUrl.hashCode() : 0);
        result = 31 * result + (foreignId != null ? foreignId.hashCode() : 0);
        result = 31 * result + (id != null ? id.hashCode() : 0);
        result = 31 * result + (mclass != null ? mclass.hashCode() : 0);
        result = 31 * result + (metaData != null ? metaData.hashCode() : 0);
        result = 31 * result + (momt != null ? momt.hashCode() : 0);
        result = 31 * result + (msgdata != null ? msgdata.hashCode() : 0);
        result = 31 * result + (mwi != null ? mwi.hashCode() : 0);
        result = 31 * result + (pid != null ? pid.hashCode() : 0);
        result = 31 * result + (receiver != null ? receiver.hashCode() : 0);
        result = 31 * result + (sender != null ? sender.hashCode() : 0);
        result = 31 * result + (service != null ? service.hashCode() : 0);
        result = 31 * result + (smsType != null ? smsType.hashCode() : 0);
        result = 31 * result + (smscId != null ? smscId.hashCode() : 0);
        result = 31 * result + (time != null ? time.hashCode() : 0);
        result = 31 * result + (udhdata != null ? udhdata.hashCode() : 0);
        result = 31 * result + (validity != null ? validity.hashCode() : 0);
        result = 31 * result + (rpi != null ? rpi.hashCode() : 0);
        result = 31 * result + (fecharegistro != null ? fecharegistro.hashCode() : 0);
        return result;
    }
}
