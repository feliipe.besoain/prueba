package com.example.demo;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.util.Arrays;

@Entity
@Table(name = "pg_default_acl", schema = "pg_catalog", catalog = "prueba2")
public class PgDefaultAclEntity {
    private Object defaclacl;
    private byte[] defaclnamespace;
    private Object defaclobjtype;
    private byte[] defaclrole;
    private byte[] oid;

    @Basic
    @Column(name = "defaclacl")
    public Object getDefaclacl() {
        return defaclacl;
    }

    public void setDefaclacl(Object defaclacl) {
        this.defaclacl = defaclacl;
    }

    @Basic
    @Column(name = "defaclnamespace")
    public byte[] getDefaclnamespace() {
        return defaclnamespace;
    }

    public void setDefaclnamespace(byte[] defaclnamespace) {
        this.defaclnamespace = defaclnamespace;
    }

    @Basic
    @Column(name = "defaclobjtype")
    public Object getDefaclobjtype() {
        return defaclobjtype;
    }

    public void setDefaclobjtype(Object defaclobjtype) {
        this.defaclobjtype = defaclobjtype;
    }

    @Basic
    @Column(name = "defaclrole")
    public byte[] getDefaclrole() {
        return defaclrole;
    }

    public void setDefaclrole(byte[] defaclrole) {
        this.defaclrole = defaclrole;
    }

    @Basic
    @Column(name = "oid")
    public byte[] getOid() {
        return oid;
    }

    public void setOid(byte[] oid) {
        this.oid = oid;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        PgDefaultAclEntity that = (PgDefaultAclEntity) o;

        if (defaclacl != null ? !defaclacl.equals(that.defaclacl) : that.defaclacl != null) return false;
        if (!Arrays.equals(defaclnamespace, that.defaclnamespace)) return false;
        if (defaclobjtype != null ? !defaclobjtype.equals(that.defaclobjtype) : that.defaclobjtype != null)
            return false;
        if (!Arrays.equals(defaclrole, that.defaclrole)) return false;
        if (!Arrays.equals(oid, that.oid)) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = defaclacl != null ? defaclacl.hashCode() : 0;
        result = 31 * result + Arrays.hashCode(defaclnamespace);
        result = 31 * result + (defaclobjtype != null ? defaclobjtype.hashCode() : 0);
        result = 31 * result + Arrays.hashCode(defaclrole);
        result = 31 * result + Arrays.hashCode(oid);
        return result;
    }
}
