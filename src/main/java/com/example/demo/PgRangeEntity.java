package com.example.demo;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.util.Arrays;

@Entity
@Table(name = "pg_range", schema = "pg_catalog", catalog = "kanellfinal2")
public class PgRangeEntity {
    private Object rngcanonical;
    private byte[] rngcollation;
    private Object rngsubdiff;
    private byte[] rngsubopc;
    private byte[] rngsubtype;
    private byte[] rngtypid;

    @Basic
    @Column(name = "rngcanonical")
    public Object getRngcanonical() {
        return rngcanonical;
    }

    public void setRngcanonical(Object rngcanonical) {
        this.rngcanonical = rngcanonical;
    }

    @Basic
    @Column(name = "rngcollation")
    public byte[] getRngcollation() {
        return rngcollation;
    }

    public void setRngcollation(byte[] rngcollation) {
        this.rngcollation = rngcollation;
    }

    @Basic
    @Column(name = "rngsubdiff")
    public Object getRngsubdiff() {
        return rngsubdiff;
    }

    public void setRngsubdiff(Object rngsubdiff) {
        this.rngsubdiff = rngsubdiff;
    }

    @Basic
    @Column(name = "rngsubopc")
    public byte[] getRngsubopc() {
        return rngsubopc;
    }

    public void setRngsubopc(byte[] rngsubopc) {
        this.rngsubopc = rngsubopc;
    }

    @Basic
    @Column(name = "rngsubtype")
    public byte[] getRngsubtype() {
        return rngsubtype;
    }

    public void setRngsubtype(byte[] rngsubtype) {
        this.rngsubtype = rngsubtype;
    }

    @Basic
    @Column(name = "rngtypid")
    public byte[] getRngtypid() {
        return rngtypid;
    }

    public void setRngtypid(byte[] rngtypid) {
        this.rngtypid = rngtypid;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        PgRangeEntity that = (PgRangeEntity) o;

        if (rngcanonical != null ? !rngcanonical.equals(that.rngcanonical) : that.rngcanonical != null) return false;
        if (!Arrays.equals(rngcollation, that.rngcollation)) return false;
        if (rngsubdiff != null ? !rngsubdiff.equals(that.rngsubdiff) : that.rngsubdiff != null) return false;
        if (!Arrays.equals(rngsubopc, that.rngsubopc)) return false;
        if (!Arrays.equals(rngsubtype, that.rngsubtype)) return false;
        if (!Arrays.equals(rngtypid, that.rngtypid)) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = rngcanonical != null ? rngcanonical.hashCode() : 0;
        result = 31 * result + Arrays.hashCode(rngcollation);
        result = 31 * result + (rngsubdiff != null ? rngsubdiff.hashCode() : 0);
        result = 31 * result + Arrays.hashCode(rngsubopc);
        result = 31 * result + Arrays.hashCode(rngsubtype);
        result = 31 * result + Arrays.hashCode(rngtypid);
        return result;
    }
}
